/*
 * Copyright (c) 2020 oil4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.telecom.oil.common.security.service;

import com.telecom.oil.common.core.constant.SecurityConstants;
import com.telecom.oil.common.core.exception.BusinessException;
import com.telecom.oil.common.core.util.R;
import com.telecom.oil.common.core.util.RetOps;
import com.telecom.oil.smart.api.entity.OilUser;
import com.telecom.oil.smart.api.feign.RemoteSmartUserService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.CacheManager;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * jianghuai
 */
@Slf4j
@RequiredArgsConstructor
public class OilWechatAppletUserDetailsServiceImpl implements OilUserDetailsService {

	private final RemoteSmartUserService remoteSmartUserService;

	private final CacheManager cacheManager;

	private UserDetails getCustomDetails(R<OilUser> result) {
		OilUser info = RetOps.of(result).getData().orElseThrow(() -> new BusinessException("客户登录失败"));
		log.debug("小程序登录用户信息:{}", info);
		// 构造security用户
		com.telecom.oil.common.security.service.OilUser oilUser = new com.telecom.oil.common.security.service.OilUser(info.getId(), null, info.getUserName(), "", info.getPhone(), info.getAvatar(),
				info.getCompany(), info.getRecognizeStatus(), true, true, true, true, AuthorityUtils.NO_AUTHORITIES);
		return oilUser;
	}

	@Override
	@SneakyThrows
	public UserDetails loadUserByUsername(String code) {
		R<OilUser> result = remoteSmartUserService.weixinLogin(code, SecurityConstants.FROM_IN);
		UserDetails userDetails = getCustomDetails(result);

		return userDetails;
	}

	/**
	 * 微信一键登录
	 * @param code
	 * @return
	 */
	@Override
	public UserDetails loadUserByCode(String code) {
		R<OilUser> result = remoteSmartUserService.weixinLogin(code, SecurityConstants.FROM_IN);
		UserDetails userDetails = getCustomDetails(result);

		return userDetails;
	}

	/**
	 * check-token 使用
	 * @param oilUser user
	 * @return
	 */
	@Override
	public UserDetails loadUserByUser(com.telecom.oil.common.security.service.OilUser oilUser) {
		return this.loadUserByUsername(oilUser.getPhone());
	}

	/**
	 * 是否支持此客户端校验
	 * @param clientId 目标客户端
	 * @return true/false
	 */
	@Override
	public boolean support(String clientId, String grantType) {
		return SecurityConstants.WECHAT.equals(grantType);
	}

}
