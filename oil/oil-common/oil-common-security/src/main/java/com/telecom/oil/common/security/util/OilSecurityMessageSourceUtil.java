package com.telecom.oil.common.security.util;

import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import java.util.Locale;

/**
 * @Author jianghuai
 * @Date 2022/12/11
 * @See org.springframework.security.core.SpringSecurityMessageSource oil 框架自身异常处理
 * 建议所有异常都是用此工具类型 便面无法复写 SpringSecuritySecurityMessageSource
 */
public class OilSecurityMessageSourceUtil extends ReloadableResourceBundleMessageSource {

	public OilSecurityMessageSourceUtil() {
		setBasename("classpath:messages/messages");
		setDefaultLocale(Locale.CHINA);
	}

	public static MessageSourceAccessor getAccessor() {
		return new MessageSourceAccessor(new OilSecurityMessageSourceUtil());
	}

}
