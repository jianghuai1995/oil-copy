DROP DATABASE IF EXISTS `oil`;

CREATE DATABASE  `oil` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8_general_ci;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

USE `oil`;

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `dept_id` bigint NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '部门名称',
  `sort_order` int NOT NULL DEFAULT '0' COMMENT '排序',
  `del_flag` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT '0' COMMENT '是否删除  -1：已删除  0：正常',
  `parent_id` bigint DEFAULT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='部门管理';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
BEGIN;
INSERT INTO `sys_dept` VALUES (1, '综合管理部', 1, '0', 0, '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11', 'admin');
INSERT INTO `sys_dept` VALUES (2, '市场经营部', 2, '0', 0, '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11', 'admin');
INSERT INTO `sys_dept` VALUES (3, '政企客户部', 3, '0', 0, '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11', 'admin');
INSERT INTO `sys_dept` VALUES (4, '云中台', 4, '0', 0, '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11', 'admin');
INSERT INTO `sys_dept` VALUES (5, '网络部', 5, '0', 0, '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11', 'admin');
INSERT INTO `sys_dept` VALUES (6, '客户服务中心', 6, '0', 0, '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11', 'admin');
COMMIT;

-- ----------------------------
-- Table structure for sys_dept_relation
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept_relation`;
CREATE TABLE `sys_dept_relation` (
  `ancestor` bigint NOT NULL,
  `descendant` bigint NOT NULL,
  PRIMARY KEY (`ancestor`,`descendant`) USING BTREE,
  KEY `idx1` (`ancestor`) USING BTREE,
  KEY `idx2` (`descendant`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='部门关系表';

-- ----------------------------
-- Records of sys_dept_relation
-- ----------------------------
BEGIN;
INSERT INTO `sys_dept_relation` (`ancestor`, `descendant`) VALUES (1, 1);
INSERT INTO `sys_dept_relation` (`ancestor`, `descendant`) VALUES (2, 2);
INSERT INTO `sys_dept_relation` (`ancestor`, `descendant`) VALUES (3, 3);
INSERT INTO `sys_dept_relation` (`ancestor`, `descendant`) VALUES (4, 4);
INSERT INTO `sys_dept_relation` (`ancestor`, `descendant`) VALUES (5, 5);
INSERT INTO `sys_dept_relation` (`ancestor`, `descendant`) VALUES (6, 6);
COMMIT;

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` bigint NOT NULL,
  `dict_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '标识',
  `description` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '描述',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  `system_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '是否是系统内置',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '删除标记',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `sys_dict_del_flag` (`del_flag`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='字典表';

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
BEGIN;
INSERT INTO `sys_dict` VALUES (1, 'dict_type', '字典类型', NULL, '0', '0', '2022-11-11 11:11:11', '', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_dict` VALUES (2, 'log_type', '日志类型', NULL, '0', '0', '2022-11-11 11:11:11', '', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_dict` VALUES (3, 'ds_type', '驱动类型', NULL, '0', '0', '2022-11-11 11:11:11', '', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_dict` VALUES (4, 'param_type', '参数配置', '检索、原文、报表、安全、文档、消息、其他', '1', '0', '2022-11-11 11:11:11', 'admin', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_dict` VALUES (5, 'status_type', '租户状态', '租户状态', '1', '0', '2022-11-11 11:11:11', 'admin', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_dict` VALUES (6, 'menu_type_status', '菜单类型', NULL, '1', '0', '2022-11-11 11:11:11', 'admin', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_dict` VALUES (7, 'dict_css_type', '字典项展示样式', NULL, '1', '0', '2022-11-11 11:11:11', 'admin', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_dict` VALUES (8, 'keepalive_status', '菜单是否开启缓冲', NULL, '1', '0', '2022-11-11 11:11:11', 'admin', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_dict` VALUES (9, 'user_lock_flag', '用户锁定标记', NULL, '1', '0', '2022-11-11 11:11:11', 'admin', 'admin', '2022-11-11 11:11:11');
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_item
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_item`;
CREATE TABLE `sys_dict_item` (
  `id` bigint NOT NULL,
  `dict_id` bigint NOT NULL COMMENT '字典ID',
  `dict_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '字典标识',
  `value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '值',
  `label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '标签',
  `type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '字典类型',
  `description` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '描述',
  `sort_order` int NOT NULL DEFAULT '0' COMMENT '排序（升序）',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT ' ' COMMENT '备注',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '删除标记',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `sys_dict_value` (`value`) USING BTREE,
  KEY `sys_dict_label` (`label`) USING BTREE,
  KEY `sys_dict_del_flag` (`del_flag`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='字典项';

-- ----------------------------
-- Records of sys_dict_item
-- ----------------------------
BEGIN;
INSERT INTO `sys_dict_item` VALUES (1, 1, 'dict_type', '1', '系统类', NULL, '系统类字典', 0, ' ', '0', '2022-11-11 11:11:11', NULL, NULL, '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (2, 1, 'dict_type', '0', '业务类', NULL, '业务类字典', 0, ' ', '0', '2022-11-11 11:11:11', NULL, NULL, '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (3, 2, 'log_type', '0', '正常', NULL, '正常', 0, ' ', '0', '2022-11-11 11:11:11', NULL, NULL, '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (4, 2, 'log_type', '9', '异常', NULL, '异常', 0, ' ', '0', '2022-11-11 11:11:11', NULL, NULL, '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (5, 3, 'ds_type', 'com.mysql.cj.jdbc.Driver', 'MYSQL8', NULL, 'MYSQL8', 0, ' ', '0', '2022-11-11 11:11:11', NULL, NULL, '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (6, 3, 'ds_type', 'com.mysql.jdbc.Driver', 'MYSQL5', NULL, 'MYSQL5', 0, ' ', '0', '2022-11-11 11:11:11', NULL, NULL, '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (7, 3, 'ds_type', 'oracle.jdbc.OracleDriver', 'Oracle', NULL, 'Oracle', 0, ' ', '0', '2022-11-11 11:11:11', NULL, NULL, '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (8, 3, 'ds_type', 'org.mariadb.jdbc.Driver', 'mariadb', NULL, 'mariadb', 0, ' ', '0', '2022-11-11 11:11:11', NULL, NULL, '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (9, 3, 'ds_type', 'com.microsoft.sqlserver.jdbc.SQLServerDriver', 'sqlserver2005+', NULL, 'sqlserver2005+', 0, ' ', '0', '2022-11-11 11:11:11', NULL, NULL, '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (10, 3, 'ds_type', 'com.microsoft.jdbc.sqlserver.SQLServerDriver', 'sqlserver2000', NULL, 'sqlserver2000', 0, ' ', '0', '2022-11-11 11:11:11', NULL, NULL, '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (11, 3, 'ds_type', 'com.ibm.db2.jcc.DB2Driver', 'db2', NULL, 'db2', 0, ' ', '0', '2022-11-11 11:11:11', NULL, NULL, '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (12, 3, 'ds_type', 'org.postgresql.Driver', 'postgresql', NULL, 'postgresql', 0, ' ', '0', '2022-11-11 11:11:11', NULL, NULL, '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (13, 4, 'param_type', '1', '检索', NULL, '检索', 0, '检索', '0', '2022-11-11 11:11:11', 'admin', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (14, 4, 'param_type', '2', '原文', NULL, '原文', 1, '原文', '0', '2022-11-11 11:11:11', 'admin', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (15, 4, 'param_type', '3', '报表', NULL, '报表', 2, '报表', '0', '2022-11-11 11:11:11', 'admin', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (16, 4, 'param_type', '4', '安全', NULL, '安全', 3, '安全', '0', '2022-11-11 11:11:11', 'admin', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (17, 4, 'param_type', '5', '文档', NULL, '文档', 4, '文档', '0', '2022-11-11 11:11:11', 'admin', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (18, 4, 'param_type', '6', '消息', NULL, '消息', 5, '消息', '0', '2022-11-11 11:11:11', 'admin', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (19, 4, 'param_type', '9', '其他', NULL, '其他', 6, '其他', '0', '2022-11-11 11:11:11', 'admin', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (20, 4, 'param_type', '0', '默认', NULL, '默认', 7, '默认', '0', '2022-11-11 11:11:11', 'admin', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (21, 5, 'status_type', '0', '正常', NULL, '状态正常', 0, '状态正常', '0', '2022-11-11 11:11:11', 'admin', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (22, 5, 'status_type', '9', '冻结', NULL, '状态冻结', 1, '状态冻结', '0', '2022-11-11 11:11:11', 'admin', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (23, 6, 'menu_type_status', '0', '菜单', NULL, '菜单', 0, ' ', '0', '2022-11-11 11:11:11', 'admin', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (24, 6, 'menu_type_status', '1', '按钮', 'success', '按钮', 1, ' ', '0', '2022-11-11 11:11:11', 'admin', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (25, 7, 'dict_css_type', 'success', 'success', 'success', 'success', 2, ' ', '0', '2022-11-11 11:11:11', 'admin', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (26, 7, 'dict_css_type', 'info', 'info', 'info', 'info', 3, ' ', '0', '2022-11-11 11:11:11', 'admin', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (27, 7, 'dict_css_type', 'warning', 'warning', 'warning', 'warning', 4, ' ', '0', '2022-11-11 11:11:11', 'admin', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (28, 7, 'dict_css_type', 'danger', 'danger', 'danger', 'danger', 5, ' ', '0', '2022-11-11 11:11:11', 'admin', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (29, 8, 'keepalive_status', '0', '否', 'info', '不开启缓冲', 0, ' ', '0', '2022-11-11 11:11:11', 'admin', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (30, 8, 'keepalive_status', '1', '是', NULL, '开启缓冲', 1, ' ', '0', '2022-11-11 11:11:11', 'admin', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (31, 9, 'user_lock_flag', '0', '正常', NULL, '正常状态', 0, ' ', '0', '2022-11-11 11:11:11', 'admin', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_dict_item` VALUES (32, 9, 'user_lock_flag', '9', '锁定', 'info', '已锁定', 9, ' ', '0', '2022-11-11 11:11:11', 'admin', 'admin', '2022-11-11 11:11:11');
COMMIT;

-- ----------------------------
-- Table structure for sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file` (
  `id` bigint NOT NULL,
  `file_name` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `bucket_name` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `original` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `type` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `file_size` bigint DEFAULT NULL COMMENT '文件大小',
  `del_flag` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT '0' COMMENT '0-正常，1-删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='文件管理表';

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` bigint NOT NULL,
  `type` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT '1' COMMENT '日志类型',
  `title` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT '' COMMENT '日志标题',
  `service_id` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '服务ID',
  `remote_addr` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '操作IP地址',
  `user_agent` varchar(1000) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '用户代理',
  `request_uri` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '请求URI',
  `method` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '操作方式',
  `params` text CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci COMMENT '操作提交的数据',
  `time` bigint DEFAULT NULL COMMENT '执行时间',
  `del_flag` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT '0' COMMENT '删除标记',
  `exception` text CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci COMMENT '异常信息',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `sys_log_create_by` (`create_by`) USING BTREE,
  KEY `sys_log_request_uri` (`request_uri`) USING BTREE,
  KEY `sys_log_type` (`type`) USING BTREE,
  KEY `sys_log_create_date` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='日志表';

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint NOT NULL,
  `name` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '菜单名称',
  `permission` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '菜单权限标识',
  `path` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '前端URL',
  `parent_id` bigint DEFAULT NULL COMMENT '父菜单ID',
  `icon` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '图标',
  `sort_order` int NOT NULL DEFAULT '0' COMMENT '排序值',
  `keep_alive` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT '0' COMMENT '0-开启，1- 关闭',
  `type` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '菜单类型 （0菜单 1按钮）',
  `del_flag` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT '0' COMMENT '逻辑删除标记(0--正常 1--删除)',
  `create_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='菜单权限表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_menu` VALUES (1000, '权限管理', NULL, '/admin', -1, 'icon-quanxianguanli', 2, '0', '0', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1100, '用户管理', NULL, '/admin/user/index', 1000, 'icon-yonghuguanli', 0, '0', '0', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1101, '用户新增', 'sys_user_add', NULL, 1100, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1102, '用户修改', 'sys_user_edit', NULL, 1100, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1103, '用户删除', 'sys_user_del', NULL, 1100, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1104, '导入导出', 'sys_user_import_export', NULL, 1100, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1200, '菜单管理', NULL, '/admin/menu/index', 1000, 'icon-caidanguanli', 1, '0', '0', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1201, '菜单新增', 'sys_menu_add', NULL, 1200, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1202, '菜单修改', 'sys_menu_edit', NULL, 1200, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1203, '菜单删除', 'sys_menu_del', NULL, 1200, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1300, '角色管理', NULL, '/admin/role/index', 1000, 'icon-jiaoseguanli', 2, '0', '0', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1301, '角色新增', 'sys_role_add', NULL, 1300, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1302, '角色修改', 'sys_role_edit', NULL, 1300, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1303, '角色删除', 'sys_role_del', NULL, 1300, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1304, '分配权限', 'sys_role_perm', NULL, 1300, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1305, '导入导出', 'sys_role_import_export', NULL, 1300, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1400, '部门管理', NULL, '/admin/dept/index', 1000, 'icon-web-icon-', 3, '0', '0', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1401, '部门新增', 'sys_dept_add', NULL, 1400, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1402, '部门修改', 'sys_dept_edit', NULL, 1400, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1403, '部门删除', 'sys_dept_del', NULL, 1400, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1500, '岗位管理', '', '/admin/post/index', 1000, 'icon-guanwang', 4, '0', '0', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1501, '岗位查看', 'sys_post_get', NULL, 1500, '1', 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1502, '岗位新增', 'sys_post_add', NULL, 1500, '1', 1, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1503, '岗位修改', 'sys_post_edit', NULL, 1500, '1', 2, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1504, '岗位删除', 'sys_post_del', NULL, 1500, '1', 3, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1505, '导入导出', 'sys_post_import_export', NULL, 1500, NULL, 4, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (2000, '系统管理', NULL, '/setting', -1, 'icon-xitongguanli', 3, '0', '0', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (2100, '日志管理', NULL, '/admin/log/index', 2000, 'icon-rizhiguanli', 3, '0', '0', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (2101, '日志删除', 'sys_log_del', NULL, 2100, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (2102, '导入导出', 'sys_log_import_export', NULL, 2100, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (2200, '字典管理', NULL, '/admin/dict/index', 2000, 'icon-navicon-zdgl', 2, '0', '0', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (2201, '字典删除', 'sys_dict_del', NULL, 2200, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (2202, '字典新增', 'sys_dict_add', NULL, 2200, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (2203, '字典修改', 'sys_dict_edit', NULL, 2200, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (2300, '令牌管理', NULL, '/admin/token/index', 2000, 'icon-denglvlingpai', 4, '0', '0', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (2301, '令牌删除', 'sys_token_del', NULL, 2300, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (2400, '终端管理', '', '/admin/client/index', 2000, 'icon-shouji', 0, '0', '0', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (2401, '客户端新增', 'sys_client_add', NULL, 2400, '1', 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (2402, '客户端修改', 'sys_client_edit', NULL, 2400, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (2403, '客户端删除', 'sys_client_del', NULL, 2400, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (2600, '文件管理', NULL, '/admin/file/index', 2000, 'icon-wenjianguanli', 1, '0', '0', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (2601, '文件删除', 'sys_file_del', NULL, 2600, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (2602, '文件新增', 'sys_file_add', NULL, 2600, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (2603, '文件修改', 'sys_file_edit', NULL, 2600, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (2700, '参数管理', NULL, '/admin/param/index', 2000, 'icon-navicon-zdgl', 5, '0', '0', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (2701, '参数新增', 'sys_publicparam_add', NULL, 2700, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (2702, '参数删除', 'sys_publicparam_del', NULL, 2700, NULL, 1, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (2703, '参数修改', 'sys_publicparam_edit', NULL, 2700, NULL, 3, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (3000, '开发平台', NULL, '/gen', -1, 'icon-jiaoseguanli', 4, '1', '0', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (3100, '数据源管理', NULL, '/gen/datasource', 3000, 'icon-mysql', 3, '1', '0', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (3200, '代码生成', NULL, '/gen/index', 3000, 'icon-weibiaoti46', 0, '0', '0', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (3300, '表单管理', NULL, '/gen/form', 3000, 'icon-record', 1, '1', '0', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (3301, '表单新增', 'gen_form_add', NULL, 3300, '', 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (3302, '表单修改', 'gen_form_edit', NULL, 3300, '', 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (3303, '表单删除', 'gen_form_del', NULL, 3300, '', 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (3400, '表单设计', NULL, '/gen/design', 3000, 'icon-biaodanbiaoqian', 2, '1', '0', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (4000, '服务监控', NULL, 'http://localhost:5001/login', -1, 'icon-jiankong', 5, '0', '0', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (9999, '系统官网', NULL, 'https://oil4cloud.com/#/', -1, 'icon-tubiaozhizuosvg-13', 999, '0', '0', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1668239830812, '加油管理', '', '/smart/oilrecord/index', 1591347686715023362, 'icon-rizhi', 3, '0', '0', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1668239830813, '加油查看', 'smart_oilrecord_get', NULL, 1668239830812, '1', 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1668239830814, '加油新增', 'smart_oilrecord_add', NULL, 1668239830812, '1', 1, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1668239830815, '加油修改', 'smart_oilrecord_edit', NULL, 1668239830812, '1', 2, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1668239830816, '加油删除', 'smart_oilrecord_del', NULL, 1668239830812, '1', 3, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1668239830872, '用户管理', '', '/smart/oiluser/index', 1591347686715023362, 'icon-yonghuguanli', 1, '0', '0', '1', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1668239830873, '用户查看', 'smart_oiluser_get', NULL, 1668239830872, '1', 0, '0', '1', '1', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1668239830874, '用户新增', 'smart_oiluser_add', NULL, 1668239830872, '1', 1, '0', '1', '1', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1668239830875, '用户修改', 'smart_oiluser_edit', NULL, 1668239830872, '1', 2, '0', '1', '1', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1668239830876, '用户删除', 'smart_oiluser_del', NULL, 1668239830872, '1', 3, '0', '1', '1', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1668239830896, 'Banner管理', '', '/smart/oilbanner/index', 1591347686715023362, 'icon-iframe', 0, '0', '0', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1668239830897, 'Banner查看', 'smart_oilbanner_get', NULL, 1668239830896, '1', 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1668239830898, 'Banner新增', 'smart_oilbanner_add', NULL, 1668239830896, '1', 1, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1668239830899, 'Banner修改', 'smart_oilbanner_edit', NULL, 1668239830896, '1', 2, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1668239830900, 'Banner删除', 'smart_oilbanner_del', NULL, 1668239830896, '1', 3, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1668239830919, '通知管理', '', '/smart/oilnotice/index', 1591347686715023362, 'icon-shouji1', 2, '0', '0', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1668239830920, '通知查看', 'smart_oilnotice_get', NULL, 1668239830919, '1', 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1668239830921, '通知新增', 'smart_oilnotice_add', NULL, 1668239830919, '1', 1, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1668239830922, '通知修改', 'smart_oilnotice_edit', NULL, 1668239830919, '1', 2, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1668239830923, '通知删除', 'smart_oilnotice_del', NULL, 1668239830919, '1', 3, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1671781500604, '文件管理表管理', '', '/smart/sysfile/index', 1591347686715023362, 'icon-rizhiguanli', 4, '0', '0', '1', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1671781500605, '文件管理表查看', 'smart_sysfile_get', NULL, 1671781500604, '1', 0, '0', '1', '1', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1671781500606, '文件管理表新增', 'smart_sysfile_add', NULL, 1671781500604, '1', 1, '0', '1', '1', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1671781500607, '文件管理表修改', 'smart_sysfile_edit', NULL, 1671781500604, '1', 2, '0', '1', '1', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1671781500608, '文件管理表删除', 'smart_sysfile_del', NULL, 1671781500604, '1', 3, '0', '1', '1', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1591347686715023362, 'C端管理', NULL, '/smart', -1, 'icon-daohanglanmoshi02', 1, '0', '0', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1591348485335670785, 'Banner图标', NULL, '/smart/banner/index', 1591347686715023362, 'icon-iframe', 0, '0', '0', '1', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1591348847236997122, '用户管理', NULL, '/smart/user/index', 1591347686715023362, 'icon-yonghuguanli', 1, '0', '0', '1', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1591349071862947841, '通知管理', NULL, '/smart/notice/index', 1591347686715023362, 'icon-shouji1', 2, '0', '0', '1', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1591349218088968194, '加油管理', NULL, '/smart/oilrecord/index', 1591347686715023362, 'icon-rizhi', 3, '0', '0', '1', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1607650981309878274, '文件管理', NULL, '/smart/sysfile/index', 1591347686715023362, 'icon-biaodansheji', 4, '0', '0', '1', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1607909516065120258, 'user管理', NULL, '/smart/oiluser/index', 1591347686715023362, 'icon-yonghu1', 6, '0', '0', '1', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1607909745065730050, 'user查看', 'smart_oiluser_get', NULL, 1607909516065120258, NULL, 0, '0', '1', '1', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1607909887844032514, 'user增加', 'smart_oiluser_add', NULL, 1607909516065120258, NULL, 1, '0', '1', '1', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1607909971994353666, 'user修改', 'smart_oiluser_edit', NULL, 1607909516065120258, NULL, 2, '0', '1', '1', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1607910063727976449, 'user删除', 'smart_oiluser_del', NULL, 1607909516065120258, NULL, 3, '0', '1', '1', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1608736785331396610, 'C端用户管理', NULL, '/smart/oiluserinfo/index', 1591347686715023362, 'icon-yonghuguanli', 5, '0', '0', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2023-03-15 21:54:37');
INSERT INTO `sys_menu` VALUES (1608737381954363393, 'user查看', 'smart_oiluserinfo_get', NULL, 1608736785331396610, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1608738665977618433, 'user新增', 'smart_oiluserinfo_add', NULL, 1608736785331396610, NULL, 1, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1608738765772693506, 'user删除', 'smart_oiluserinfo_del', NULL, 1608736785331396610, NULL, 3, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1608738862451400706, 'user修改', 'smart_oiluserinfo_edit', NULL, 1608736785331396610, NULL, 2, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1608744571251466242, '用户导出', 'smart_oiluserinfo_export', NULL, 1608736785331396610, NULL, 4, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1610809452947578882, '加油导出', 'smart_oilrecord_export', NULL, 1668239830812, NULL, 4, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1610809665024172033, '通知导出', 'smart_oilnotice_export', NULL, 1668239830919, NULL, 4, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11');
INSERT INTO `sys_menu` VALUES (1623238399232692225, '数字乡村管理', NULL, '/smart/setrecord/index', 1591347686715023362, 'icon-tubiao1', 4, '0', '0', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2023-03-01 17:19:29');
INSERT INTO `sys_menu` VALUES (1623239016349028354, '套餐查看', 'smart_setrecord_get', NULL, 1623238399232692225, NULL, 0, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2023-03-01 17:19:45');
INSERT INTO `sys_menu` VALUES (1623239169722142721, '套餐新增', 'smart_setrecord_add', '', 1623238399232692225, NULL, 1, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2023-03-01 17:19:55');
INSERT INTO `sys_menu` VALUES (1623239238143823873, '套餐修改', 'smart_setrecord_edit', '', 1623238399232692225, NULL, 2, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2023-03-01 17:20:03');
INSERT INTO `sys_menu` VALUES (1623239320511565825, '套餐删除', 'smart_setrecord_del', '', 1623238399232692225, NULL, 3, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2023-03-01 17:20:14');
INSERT INTO `sys_menu` VALUES (1623239390153789441, '套餐导出', 'smart_setrecord_export', '', 1623238399232692225, NULL, 4, '0', '1', '0', 'admin', '2022-11-11 11:11:11', 'admin', '2023-03-01 17:20:25');
COMMIT;

-- ----------------------------
-- Table structure for sys_oauth_client_details
-- ----------------------------
DROP TABLE IF EXISTS `sys_oauth_client_details`;
CREATE TABLE `sys_oauth_client_details` (
  `client_id` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '客户端ID',
  `resource_ids` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '资源列表',
  `client_secret` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '客户端密钥',
  `scope` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '域',
  `authorized_grant_types` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '认证类型',
  `web_server_redirect_uri` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '重定向地址',
  `authorities` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '角色列表',
  `access_token_validity` int DEFAULT NULL COMMENT 'token 有效期',
  `refresh_token_validity` int DEFAULT NULL COMMENT '刷新令牌有效期',
  `additional_information` varchar(4096) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '令牌扩展字段JSON',
  `autoapprove` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '是否自动放行',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`client_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='终端信息表';

-- ----------------------------
-- Records of sys_oauth_client_details
-- ----------------------------
BEGIN;
INSERT INTO `sys_oauth_client_details` VALUES ('app', NULL, 'app', 'server', 'app,refresh_token', NULL, NULL, NULL, NULL, NULL, 'true', '2022-11-11 11:11:11', '2022-11-11 11:11:11', 'admin', 'admin');
INSERT INTO `sys_oauth_client_details` VALUES ('applet', NULL, 'applet', 'server', 'password,app,refresh_token,authorization_code,client_credentials,wechat', 'http://localhost:4040/sso1/login,http://localhost:4041/sso1/login', NULL, NULL, NULL, NULL, 'true', '2022-11-11 11:11:11', '2022-11-11 11:11:11', 'admin', 'admin');
INSERT INTO `sys_oauth_client_details` VALUES ('daemon', NULL, 'daemon', 'server', 'password,refresh_token', NULL, NULL, NULL, NULL, NULL, 'true', '2022-11-11 11:11:11', '2022-11-11 11:11:11', 'admin', 'admin');
INSERT INTO `sys_oauth_client_details` VALUES ('gen', NULL, 'gen', 'server', 'password,refresh_token', NULL, NULL, NULL, NULL, NULL, 'true', '2022-11-11 11:11:11', '2022-11-11 11:11:11', 'admin', 'admin');
INSERT INTO `sys_oauth_client_details` VALUES ('pig', NULL, 'pig', 'server', 'password,app,refresh_token,authorization_code,client_credentials', 'http://localhost:4040/sso1/login,http://localhost:4041/sso1/login', NULL, NULL, NULL, NULL, 'true', '2022-11-11 11:11:11', '2022-11-11 11:11:11', 'admin', 'admin');
INSERT INTO `sys_oauth_client_details` VALUES ('test', NULL, 'test', 'server', 'password,app,refresh_token', NULL, NULL, NULL, NULL, NULL, 'true', '2022-11-11 11:11:11', '2022-11-11 11:11:11', 'admin', 'admin');
COMMIT;

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post` (
  `post_id` bigint NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int NOT NULL COMMENT '岗位排序',
  `del_flag` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '0' COMMENT '是否删除  -1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT '' COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT '' COMMENT '更新人',
  `remark` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COMMENT='岗位信息表';

-- ----------------------------
-- Records of sys_post
-- ----------------------------
BEGIN;
INSERT INTO `sys_post` VALUES (1, 'user', '员工', 2, '0', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11', 'admin', '员工');
INSERT INTO `sys_post` VALUES (3, 'boss', '总经理', -1, '0', '2022-11-11 11:11:11', 'admin', '2022-11-11 11:11:11', 'admin', '总经理');
COMMIT;

-- ----------------------------
-- Table structure for sys_public_param
-- ----------------------------
DROP TABLE IF EXISTS `sys_public_param`;
CREATE TABLE `sys_public_param` (
  `public_id` bigint NOT NULL COMMENT '编号',
  `public_name` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `public_key` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `public_value` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `status` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT '0',
  `validate_code` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `create_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT ' ' COMMENT '创建人',
  `update_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT ' ' COMMENT '修改人',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `public_type` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT '0',
  `system_flag` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT '0',
  PRIMARY KEY (`public_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='公共参数配置表';

-- ----------------------------
-- Records of sys_public_param
-- ----------------------------
BEGIN;
INSERT INTO `sys_public_param` VALUES (1, '接口文档不显示的字段', 'GEN_HIDDEN_COLUMNS', 'tenant_id', '0', '', 'admin', 'admin', '2022-11-11 11:11:11', '2022-11-11 11:11:11', '9', '1');
INSERT INTO `sys_public_param` VALUES (2, '注册用户默认角色', 'USER_DEFAULT_ROLE', 'GENERAL_USER', '0', '', 'admin', 'admin', '2022-11-11 11:11:11', '2022-11-11 11:11:11', '2', '1');
COMMIT;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` bigint NOT NULL,
  `role_name` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `role_code` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `role_desc` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `del_flag` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT '0' COMMENT '删除标识（0-正常,1-删除）',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '修改人',
  `create_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`role_id`) USING BTREE,
  UNIQUE KEY `role_idx1_role_code` (`role_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='系统角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_role` VALUES (1, '管理员', 'ROLE_ADMIN', '管理员', '0', '2022-11-11 11:11:11', '2022-11-11 11:11:11', 'admin', 'admin');
INSERT INTO `sys_role` VALUES (2, '普通用户', 'GENERAL_USER', '普通用户', '0', '2022-11-11 11:11:11', '2022-11-11 11:11:11', 'admin', 'admin');
COMMIT;

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` bigint NOT NULL,
  `menu_id` bigint NOT NULL,
  PRIMARY KEY (`role_id`,`menu_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='角色菜单表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_role_menu` VALUES (1, 1000);
INSERT INTO `sys_role_menu` VALUES (1, 1100);
INSERT INTO `sys_role_menu` VALUES (1, 1101);
INSERT INTO `sys_role_menu` VALUES (1, 1102);
INSERT INTO `sys_role_menu` VALUES (1, 1103);
INSERT INTO `sys_role_menu` VALUES (1, 1104);
INSERT INTO `sys_role_menu` VALUES (1, 1200);
INSERT INTO `sys_role_menu` VALUES (1, 1201);
INSERT INTO `sys_role_menu` VALUES (1, 1202);
INSERT INTO `sys_role_menu` VALUES (1, 1203);
INSERT INTO `sys_role_menu` VALUES (1, 1300);
INSERT INTO `sys_role_menu` VALUES (1, 1301);
INSERT INTO `sys_role_menu` VALUES (1, 1302);
INSERT INTO `sys_role_menu` VALUES (1, 1303);
INSERT INTO `sys_role_menu` VALUES (1, 1304);
INSERT INTO `sys_role_menu` VALUES (1, 1305);
INSERT INTO `sys_role_menu` VALUES (1, 1400);
INSERT INTO `sys_role_menu` VALUES (1, 1401);
INSERT INTO `sys_role_menu` VALUES (1, 1402);
INSERT INTO `sys_role_menu` VALUES (1, 1403);
INSERT INTO `sys_role_menu` VALUES (1, 1500);
INSERT INTO `sys_role_menu` VALUES (1, 1501);
INSERT INTO `sys_role_menu` VALUES (1, 1502);
INSERT INTO `sys_role_menu` VALUES (1, 1503);
INSERT INTO `sys_role_menu` VALUES (1, 1504);
INSERT INTO `sys_role_menu` VALUES (1, 1505);
INSERT INTO `sys_role_menu` VALUES (1, 2000);
INSERT INTO `sys_role_menu` VALUES (1, 2100);
INSERT INTO `sys_role_menu` VALUES (1, 2101);
INSERT INTO `sys_role_menu` VALUES (1, 2102);
INSERT INTO `sys_role_menu` VALUES (1, 2200);
INSERT INTO `sys_role_menu` VALUES (1, 2201);
INSERT INTO `sys_role_menu` VALUES (1, 2202);
INSERT INTO `sys_role_menu` VALUES (1, 2203);
INSERT INTO `sys_role_menu` VALUES (1, 2300);
INSERT INTO `sys_role_menu` VALUES (1, 2301);
INSERT INTO `sys_role_menu` VALUES (1, 2400);
INSERT INTO `sys_role_menu` VALUES (1, 2401);
INSERT INTO `sys_role_menu` VALUES (1, 2402);
INSERT INTO `sys_role_menu` VALUES (1, 2403);
INSERT INTO `sys_role_menu` VALUES (1, 2600);
INSERT INTO `sys_role_menu` VALUES (1, 2601);
INSERT INTO `sys_role_menu` VALUES (1, 2602);
INSERT INTO `sys_role_menu` VALUES (1, 2603);
INSERT INTO `sys_role_menu` VALUES (1, 2700);
INSERT INTO `sys_role_menu` VALUES (1, 2701);
INSERT INTO `sys_role_menu` VALUES (1, 2702);
INSERT INTO `sys_role_menu` VALUES (1, 2703);
INSERT INTO `sys_role_menu` VALUES (1, 3000);
INSERT INTO `sys_role_menu` VALUES (1, 3100);
INSERT INTO `sys_role_menu` VALUES (1, 3200);
INSERT INTO `sys_role_menu` VALUES (1, 3300);
INSERT INTO `sys_role_menu` VALUES (1, 3301);
INSERT INTO `sys_role_menu` VALUES (1, 3302);
INSERT INTO `sys_role_menu` VALUES (1, 3303);
INSERT INTO `sys_role_menu` VALUES (1, 3400);
INSERT INTO `sys_role_menu` VALUES (1, 4000);
INSERT INTO `sys_role_menu` VALUES (1, 9999);
INSERT INTO `sys_role_menu` VALUES (1, 1668239830812);
INSERT INTO `sys_role_menu` VALUES (1, 1668239830813);
INSERT INTO `sys_role_menu` VALUES (1, 1668239830814);
INSERT INTO `sys_role_menu` VALUES (1, 1668239830815);
INSERT INTO `sys_role_menu` VALUES (1, 1668239830816);
INSERT INTO `sys_role_menu` VALUES (1, 1668239830896);
INSERT INTO `sys_role_menu` VALUES (1, 1668239830897);
INSERT INTO `sys_role_menu` VALUES (1, 1668239830898);
INSERT INTO `sys_role_menu` VALUES (1, 1668239830899);
INSERT INTO `sys_role_menu` VALUES (1, 1668239830900);
INSERT INTO `sys_role_menu` VALUES (1, 1668239830919);
INSERT INTO `sys_role_menu` VALUES (1, 1668239830920);
INSERT INTO `sys_role_menu` VALUES (1, 1668239830921);
INSERT INTO `sys_role_menu` VALUES (1, 1668239830922);
INSERT INTO `sys_role_menu` VALUES (1, 1668239830923);
INSERT INTO `sys_role_menu` VALUES (1, 1591347686715023362);
INSERT INTO `sys_role_menu` VALUES (1, 1608736785331396610);
INSERT INTO `sys_role_menu` VALUES (1, 1608737381954363393);
INSERT INTO `sys_role_menu` VALUES (1, 1608738665977618433);
INSERT INTO `sys_role_menu` VALUES (1, 1608738765772693506);
INSERT INTO `sys_role_menu` VALUES (1, 1608738862451400706);
INSERT INTO `sys_role_menu` VALUES (1, 1608744571251466242);
INSERT INTO `sys_role_menu` VALUES (1, 1610809452947578882);
INSERT INTO `sys_role_menu` VALUES (1, 1610809665024172033);
INSERT INTO `sys_role_menu` VALUES (1, 1623238399232692225);
INSERT INTO `sys_role_menu` VALUES (1, 1623239016349028354);
INSERT INTO `sys_role_menu` VALUES (1, 1623239169722142721);
INSERT INTO `sys_role_menu` VALUES (1, 1623239238143823873);
INSERT INTO `sys_role_menu` VALUES (1, 1623239320511565825);
INSERT INTO `sys_role_menu` VALUES (1, 1623239390153789441);
INSERT INTO `sys_role_menu` VALUES (2, 1668239830812);
INSERT INTO `sys_role_menu` VALUES (2, 1668239830813);
INSERT INTO `sys_role_menu` VALUES (2, 1668239830814);
INSERT INTO `sys_role_menu` VALUES (2, 1668239830815);
INSERT INTO `sys_role_menu` VALUES (2, 1668239830816);
INSERT INTO `sys_role_menu` VALUES (2, 1668239830896);
INSERT INTO `sys_role_menu` VALUES (2, 1668239830897);
INSERT INTO `sys_role_menu` VALUES (2, 1668239830898);
INSERT INTO `sys_role_menu` VALUES (2, 1668239830899);
INSERT INTO `sys_role_menu` VALUES (2, 1668239830900);
INSERT INTO `sys_role_menu` VALUES (2, 1668239830919);
INSERT INTO `sys_role_menu` VALUES (2, 1668239830920);
INSERT INTO `sys_role_menu` VALUES (2, 1668239830921);
INSERT INTO `sys_role_menu` VALUES (2, 1668239830922);
INSERT INTO `sys_role_menu` VALUES (2, 1668239830923);
INSERT INTO `sys_role_menu` VALUES (2, 1591347686715023362);
INSERT INTO `sys_role_menu` VALUES (2, 1608736785331396610);
INSERT INTO `sys_role_menu` VALUES (2, 1608737381954363393);
INSERT INTO `sys_role_menu` VALUES (2, 1608738665977618433);
INSERT INTO `sys_role_menu` VALUES (2, 1608738765772693506);
INSERT INTO `sys_role_menu` VALUES (2, 1608738862451400706);
INSERT INTO `sys_role_menu` VALUES (2, 1608744571251466242);
INSERT INTO `sys_role_menu` VALUES (2, 1610809452947578882);
INSERT INTO `sys_role_menu` VALUES (2, 1610809665024172033);
INSERT INTO `sys_role_menu` VALUES (2, 1623238399232692225);
INSERT INTO `sys_role_menu` VALUES (2, 1623239016349028354);
INSERT INTO `sys_role_menu` VALUES (2, 1623239169722142721);
INSERT INTO `sys_role_menu` VALUES (2, 1623239238143823873);
INSERT INTO `sys_role_menu` VALUES (2, 1623239320511565825);
INSERT INTO `sys_role_menu` VALUES (2, 1623239390153789441);
COMMIT;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint NOT NULL,
  `username` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '密码',
  `salt` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '随机盐',
  `phone` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '简介',
  `avatar` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '头像',
  `dept_id` bigint DEFAULT NULL COMMENT '部门ID',
  `lock_flag` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT '0' COMMENT '0-正常，9-锁定',
  `del_flag` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT '0' COMMENT '0-正常，1-删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`user_id`) USING BTREE,
  KEY `user_idx1_username` (`username`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='用户表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
BEGIN;
INSERT INTO `sys_user` VALUES (1, 'admin', '$2a$10$VmPerFhXeRjRwEw1ChvLRe29MEaW8oZ6gRjgjWSs5ia3CTnWYRUgS', NULL, '18888888888', '/admin/sys-file/oil-admin/514adfb713f241739e05657100650063.jpg', 4, '0', '0', '2022-11-11 11:11:11', '2023-03-10 15:56:03', NULL, 'admin');
COMMIT;

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post` (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `post_id` bigint NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`,`post_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='用户与岗位关联表';

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (1591360741297479682, 1);
INSERT INTO `sys_user_post` VALUES (1635991872326828033, 1);
COMMIT;

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` bigint NOT NULL,
  `role_id` bigint NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='用户角色表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (1591360741297479682, 2);
INSERT INTO `sys_user_role` VALUES (1635991872326828033, 2);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
