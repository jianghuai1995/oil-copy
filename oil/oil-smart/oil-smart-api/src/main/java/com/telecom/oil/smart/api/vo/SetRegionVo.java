package com.telecom.oil.smart.api.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SetRegionVo implements Serializable {

	@Schema(description ="区域ID")
	private Integer id;

	@Schema(description ="名称")
	private String name;

}
