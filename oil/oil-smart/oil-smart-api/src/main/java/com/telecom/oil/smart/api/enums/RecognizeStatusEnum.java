package com.telecom.oil.smart.api.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum RecognizeStatusEnum {

	UN_RECOGNIZE(0, "未认证"),
	TOBE_RECOGNIZE(1, "认证中"),
	RECOGNIZED(2, "已认证"),
	FAIL_RECOGNIZE(3, "认证不通过");

	private final Integer code;

	private final String description;

}
