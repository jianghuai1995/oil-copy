package com.telecom.oil.smart.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Schema(name = "批量删除Req")
public class OilBannerBatchDelReq implements Serializable {

	private static final long serialVersionID = 1L;

	@Schema(name = "ids")
	private List<Integer> ids;

}
