package com.telecom.oil.smart.api.enums;

public enum LockFlagEnum {

	LOCK(0,"锁定"),
	NORMAL(1, "正常");

	private Integer flag;

	private String desc;

	LockFlagEnum(Integer flag, String desc) {
		this.flag = flag;
		this.desc = desc;
	}

	public Integer getFlag() {
		return flag;
	}

	public String getDesc() {
		return desc;
	}
}
