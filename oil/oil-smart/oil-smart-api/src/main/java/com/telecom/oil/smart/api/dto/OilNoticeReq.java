package com.telecom.oil.smart.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Schema(name = "通知记录req")
public class OilNoticeReq extends PageRequest implements Serializable {

	private static final long serialVersionID = 1L;

	@Schema(name = "标题")
	private String title;

}
