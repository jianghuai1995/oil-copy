/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.telecom.oil.smart.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.telecom.oil.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 营销套餐表
 *
 * @author jianghuai
 * @date 2023-05-01 21:13:52
 */
@Data
@TableName("set_record_market")
@EqualsAndHashCode(callSuper = true)
@Schema(description = "营销套餐表")
public class SetRecordMarket extends BaseEntity {

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(description ="主键id")
    private Long id;

    /**
     * 用户id
     */
    @Schema(description ="用户id")
    private Long userId;

    /**
     * 营销类别: NEW-新装; SAVE-保存; OTHER-其他 
     */
    @Schema(description ="营销类别: NEW-新装; SAVE-保存; OTHER-其他 ")
    private String businessType;

    /**
     * 系统揽收人
     */
    @Schema(description ="系统揽收人")
    private String receiver;

    /**
     * 办理日期
     */
    @Schema(description ="办理日期")
    private LocalDateTime signDate;

    /**
     * 原套餐
     */
    @Schema(description ="原套餐")
    private String oldSet;

    /**
     * 办理套餐
     */
    @Schema(description ="办理套餐")
    private String signSet;

    /**
     * 系统优惠
     */
    @Schema(description ="系统优惠")
    private String systemDiscount;

    /**
     * 调账金额
     */
    @Schema(description ="调账金额")
    private BigDecimal adjustAmount;

    /**
     * 甩单编号
     */
    @Schema(description ="甩单编号")
    private String orderCode;

    /**
     * 营销积分
     */
    @Schema(description ="营销积分")
    private Integer marketPoint;

    /**
     * 主卡号码
     */
    @Schema(description ="主卡号码")
    private String masterCard;

    /**
     * 副卡1
     */
    @Schema(description ="副卡1")
    private String assistantCard1;

    /**
     * 副卡2
     */
    @Schema(description ="副卡2")
    private String assistantCard2;

    /**
     * 副卡3
     */
    @Schema(description ="副卡3")
    private String assistantCard3;

    /**
     * 副卡4
     */
    @Schema(description ="副卡4")
    private String assistantCard4;


}
