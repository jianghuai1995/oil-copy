/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.telecom.oil.smart.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.telecom.oil.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户信息表
 *
 * @author jianghuai
 * @date 2022-10-18 14:39:08
 */
@Data
@TableName("oil_user")
@EqualsAndHashCode(callSuper = true)
@Schema(description = "用户信息表")
public class OilUser extends BaseEntity {

	/**
	 * 主键id
	 */
	@TableId(type = IdType.ASSIGN_ID)
	@Schema(description = "主键id")
	private Long id;

	/**
	 * 微信账户
	 */
	@Schema(description = "微信账户")
	private String weixinAccount;

	/**
	 * 电话号码
	 */
	@Schema(description = "电话号码")
	private String phone;

	/**
	 * 头像
	 */
	@Schema(description = "头像")
	private String avatar;

	/**
	 * 用户名
	 */
	@Schema(description = "用户名")
	private String userName;

	/**
	 * 昵称
	 */
	@Schema(description = "昵称")
	private String nickName;

	/**
	 * 性别
	 */
	@Schema(description = "性别")
	private String sex;

	/**
	 * 地址
	 */
	@Schema(description = "地址")
	private String address;

	/**
	 * email地址
	 */
	@Schema(description = "email地址")
	private String email;

	/**
	 * 公司名称
	 */
	@Schema(description = "公司名称")
	private String company;

	/**
	 * 部门id
	 */
	@Schema(description = "部门id")
	private Integer deptId;

	/**
	 * unionId
	 */
	@Schema(description = "unionId")
	private String unionId;

	/**
	 * openId
	 */
	@Schema(description = "openId")
	private String openId;

	/**
	 * 认证状态：0-未认证；1-认证中；2-已认证；3-认证不通过
	 */
	@Schema(description = "认证状态：0-未认证；1-认证中；2-已认证；3-认证不通过")
	private Integer recognizeStatus;

}
