package com.telecom.oil.smart.api.vo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.pig4cloud.plugin.excel.annotation.ExcelLine;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 套餐记录excel 对应的实体
 *
 * @author fxz
 * @date 2022/3/21
 */
@Data
@ColumnWidth(30)
public class SetRecordExcelVo implements Serializable {

	private static final long serialVersionUID = 1L;

	@ExcelLine
	@ExcelIgnore
	private Long lineNum;

	@ExcelProperty("套餐编号")
	private Long id;

	@NotBlank(message = "用户名不能为空")
	@ExcelProperty("用户名")
	private String userName;

	@ExcelProperty("乡镇、街道名称")
	private String level2Name;

	@ExcelProperty("村、社区名称")
	private String level3Name;

	@NotBlank(message = "组别不能为空")
	@ExcelProperty("组别")
	private Integer houseGroup;

	@ExcelProperty("门牌号")
	private Integer houseNumber;

	@ExcelProperty("户主")
	private String houseHolder;

	@ExcelProperty("是否常住")
	private String permanentDetail;

	@ExcelProperty("电话号码")
	private String phone;

	@ExcelProperty("运营商")
	private String op;

	@ExcelProperty("家庭通信消费支出（单位：元/月）")
	private BigDecimal houseCConsume;

	@ExcelProperty("当前套餐")
	private String currentSet;

	@ExcelProperty("当前套餐到期时间")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate menuExpireTime;

	@ExcelProperty("走访情况")
	private String visitDetails;

	@ExcelProperty("详细地址")
	private String address;

	@ExcelProperty("备注")
	private String remark;

	@ExcelProperty(value = "创建时间")
	private LocalDateTime createTime;

}
