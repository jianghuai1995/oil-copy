package com.telecom.oil.smart.api.enums;

public enum MarketTypeEnum {

	NEW("新装"),

	SAVE("保存"),

	OTHER("其他"),
	;

	private String desc;

	MarketTypeEnum(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}

}
