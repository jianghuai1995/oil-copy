package com.telecom.oil.smart.api.enums;

public enum MiniExceptionEnum {

	UN_LOGIN("1401", "未登录"),
	UN_AUTHENTICATED("1402", "未认证");

	private final String code;
	private final String msg;

	MiniExceptionEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}

	public String getMsg() {
		return msg;
	}
}
