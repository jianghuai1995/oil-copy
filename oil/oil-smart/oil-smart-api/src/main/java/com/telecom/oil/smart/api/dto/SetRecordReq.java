package com.telecom.oil.smart.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Schema(name = "套餐记录req")
public class SetRecordReq extends PageRequest implements Serializable {

	private static final long serialVersionID = 1L;

	@Schema(name = "联系电话")
	private String phone;

	@Schema(name = "户主")
	private String houseHolder;

	@Schema(name = "用户ID")
	private Long userId;

}
