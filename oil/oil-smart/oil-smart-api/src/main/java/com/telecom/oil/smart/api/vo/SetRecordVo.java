package com.telecom.oil.smart.api.vo;

import com.telecom.oil.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SetRecordVo extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Schema(description = "主键id")
	private Long id;

	@Schema(description = "用户id")
	private Long userId;

	@Schema(description = "用户名")
	private String userName;

	@Schema(description ="乡镇、街道编码")
	private Integer level2Code;

	@Schema(description ="乡镇、街道名称")
	private String level2Name;

	@Schema(description ="村、社区编码")
	private Integer level3Code;

	@Schema(description ="村、社区名称")
	private String level3Name;

	@Schema(description ="组别")
	private Integer houseGroup;

	@Schema(description ="门牌号")
	private Integer houseNumber;

	@Schema(description ="户主")
	private String houseHolder;

	@Schema(description ="是否常住：1-是；0-否")
	private Integer permanent;

	@Schema(description ="是否常住")
	private String permanentDetail;

	@Schema(description ="电话号码")
	private String phone;

	@Schema(description ="运营商")
	private String op;

	@Schema(description ="家庭通信消费支出（单位：元/月）")
	private BigDecimal houseCConsume;

	@Schema(description ="当前套餐")
	private String currentSet;

	@Schema(description ="当前套餐到期时间")
	private Date menuExpireTime;

	@Schema(description ="走访情况")
	private String visitDetails;

	@Schema(description ="现场照片")
	private String siteImage;

	@Schema(description ="详细地址")
	private String address;

	@Schema(description ="备注")
	private String remark;

	@Schema(description ="经度")
	private BigDecimal longitude;

	@Schema(description ="纬度")
	private BigDecimal latitude;

}
