/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.telecom.oil.smart.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.telecom.oil.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户组织表
 *
 * @author jianghuai
 * @date 2022-10-18 14:39:08
 */
@Data
@TableName("oil_org_info")
@EqualsAndHashCode(callSuper = true)
@Schema(description = "用户组织表")
public class OilOrgInfo extends BaseEntity {

	/**
	 * 主键id
	 */
	@TableId(type = IdType.ASSIGN_ID)
	@Schema(description = "主键id")
	private Integer id;

	/**
	 * 部门名称
	 */
	@Schema(description = "部门名称")
	private String deptName;

	/**
	 * 父级id
	 */
	@Schema(description = "父级id")
	private Integer parentId;

	/**
	 * 排序
	 */
	@Schema(description = "排序")
	private Integer sortOrder;

	/**
	 * 是否删除 -1：已删除 0：正常
	 */
	@Schema(description = "是否删除  -1：已删除  0：正常")
	private String delFlag;

}
