package com.telecom.oil.smart.api.vo;

import com.telecom.oil.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OilUserSimpleVo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Schema(description = "电话号码")
	private String phone;

	@Schema(description = "用户名")
	private String userName;

}
