package com.telecom.oil.smart.api.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Date;

@Data
public class SetRecordWithMarketReq {

	@Schema(description ="主键id")
	private Long id;

	@Schema(description = "用户id")
	private Long userId;

	@Schema(description ="乡镇、街道编码")
	private Integer level2Code;

	@Schema(description ="村、社区编码")
	private Integer level3Code;

	@Schema(description ="组别")
	private Integer houseGroup;

	@Schema(description ="门牌号")
	private Integer houseNumber;

	@Schema(description ="户主")
	private String houseHolder;

	@Schema(description ="是否常住：1-是；0-否")
	private Integer permanent;

	@Schema(description ="电话号码")
	private String phone;

	@Schema(description ="运营商")
	private String op;

	@Schema(description ="家庭通信消费支出（单位：元/月）")
	private BigDecimal houseCConsume;

	@Schema(description ="当前套餐")
	private String currentSet;

	@Schema(description ="当前套餐到期时间")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date menuExpireTime;

	@Schema(description ="走访情况")
	private String visitDetails;

	@Schema(description ="现场照片")
	private String siteImage;

	@Schema(description ="详细地址")
	private String address;

	@Schema(description ="备注")
	private String remark;

	@Schema(description ="经度")
	private BigDecimal longitude;

	@Schema(description ="纬度")
	private BigDecimal latitude;

	@Schema(description = "套餐营销req")
	private SetRecordMarketReq market;

}
