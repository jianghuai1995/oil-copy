/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.telecom.oil.smart.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.telecom.oil.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * 
 *
 * @author jianghuai
 * @date 2023-05-06 15:16:46
 */
@Data
@TableName("oil_car")
@EqualsAndHashCode(callSuper = true)
@Schema(description = "车辆信息表")
public class OilCar extends BaseEntity {

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(description ="主键id")
    private Long id;

    /**
     * 车牌号
     */
    @Schema(description ="车牌号")
    private String carNumber;

    /**
     * 车辆登记日期
     */
    @Schema(description ="车辆登记日期")
	@JsonFormat(pattern = "yyyy-MM-dd")
    private Date registerDate;

    /**
     * 车辆类型: TRUCK-皮卡; SEDEN-轿车; BUSINESS-商务车; FREIGHT-货车; OFFROAD-越野车; PROMOTION-宣传车
     */
    @Schema(description ="车辆类型: TRUCK-皮卡; SEDEN-轿车; BUSINESS-商务车; FREIGHT-货车; OFFROAD-越野车; PROMOTION-宣传车")
    private String carType;

    /**
     * 车辆品牌
     */
    @Schema(description ="车辆品牌")
    private String carBrand;

    /**
     * 是否安装ETC：1-是; 0-否
     */
    @Schema(description ="是否安装ETC：1-是; 0-否")
    private Integer carEtc;

    /**
     * 是否安装GPS：1-是; 0-否
     */
    @Schema(description ="是否安装GPS：1-是; 0-否")
    private Integer carGps;

    /**
     * 责任人（后勤支撑及兼职驾驶员）
     */
    @Schema(description ="责任人（后勤支撑及兼职驾驶员）")
    private String contactPerson;

    /**
     * 备注
     */
    @Schema(description ="备注")
    private String remark;


}
