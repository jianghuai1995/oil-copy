package com.telecom.oil.smart.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Schema(name = "OCR识别req")
public class OcrRecognizeReq implements Serializable {

	private static final long serialVersionID = 1L;

	@Schema(name = "图像URL")
	String url;

}
