/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.telecom.oil.smart.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.telecom.oil.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDateTime;

/**
 * 套餐信息调查表
 *
 * @author jianghuai
 * @date 2023-02-07 11:55:00
 */
@Data
@TableName("set_record")
@EqualsAndHashCode(callSuper = true)
@Schema(description = "套餐信息调查表")
public class SetRecord extends BaseEntity {

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(description ="主键id")
    private Long id;

	/**
	 * 用户id
	 */
	@Schema(description = "用户id")
	private Long userId;

    /**
     * 乡镇、街道编码
     */
    @Schema(description ="乡镇、街道编码")
    private Integer level2Code;

    /**
     * 村、社区编码
     */
    @Schema(description ="村、社区编码")
    private Integer level3Code;

    /**
     * 组别
     */
    @Schema(description ="组别")
    private Integer houseGroup;

    /**
     * 门牌号
     */
    @Schema(description ="门牌号")
    private Integer houseNumber;

    /**
     * 户主
     */
    @Schema(description ="户主")
    private String houseHolder;

    /**
     * 是否常住：1-是；0-否
     */
    @Schema(description ="是否常住：1-是；0-否")
    private Integer permanent;

    /**
     * 电话号码
     */
    @Schema(description ="电话号码")
    private String phone;

    /**
     * 运营商
     */
    @Schema(description ="运营商")
    private String op;

    /**
     * 家庭通信消费支出（单位：元/月）
     */
    @Schema(description ="家庭通信消费支出（单位：元/月）")
    private BigDecimal houseCConsume;

    /**
     * 当前套餐
     */
    @Schema(description ="当前套餐")
    private String currentSet;

    /**
     * 当前套餐到期时间
     */
    @Schema(description ="当前套餐到期时间")
	@JsonFormat(pattern = "yyyy-MM-dd")
    private Date menuExpireTime;

    /**
     * 走访情况
     */
    @Schema(description ="走访情况")
    private String visitDetails;

    /**
     * 现场照片
     */
    @Schema(description ="现场照片")
    private String siteImage;

    /**
     * 详细地址
     */
    @Schema(description ="详细地址")
    private String address;

    /**
     * 备注
     */
    @Schema(description ="备注")
    private String remark;

    /**
     * 经度
     */
    @Schema(description ="经度")
    private BigDecimal longitude;

    /**
     * 纬度
     */
    @Schema(description ="纬度")
    private BigDecimal latitude;


}
