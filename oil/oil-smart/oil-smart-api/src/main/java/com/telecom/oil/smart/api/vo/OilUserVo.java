package com.telecom.oil.smart.api.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.telecom.oil.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OilUserVo extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Schema(description = "主键id")
	private Long id;

	@Schema(description = "微信账户")
	private String weixinAccount;

	@Schema(description = "电话号码")
	private String phone;

	@Schema(description = "头像")
	private String avatar;

	@Schema(description = "用户名")
	private String userName;

	@Schema(description = "昵称")
	private String nickName;

	@Schema(description = "性别")
	private String sex;

	@Schema(description = "地址")
	private String address;

	@Schema(description = "email地址")
	private String email;

	@Schema(description = "公司名称")
	private String company;

	@Schema(description = "部门id")
	private Integer deptId;

	@Schema(description = "unionId")
	private String unionId;

	@Schema(description = "openId")
	private String openId;

	@Schema(description = "认证状态：0-未认证；1-认证中；2-认证通过；3-认证不通过")
	private Integer recognizeStatus;

}
