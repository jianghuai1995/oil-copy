package com.telecom.oil.smart.api.exception;

public class MiniBusinessException extends RuntimeException{

	private String errCode;

	public MiniBusinessException(String errCode, String message) {
		super(message);
		this.errCode = errCode;
	}

	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}
}
