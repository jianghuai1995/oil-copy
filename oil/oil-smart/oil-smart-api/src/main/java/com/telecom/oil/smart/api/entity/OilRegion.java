/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.telecom.oil.smart.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.telecom.oil.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 行政区域表
 *
 * @author jianghuai
 * @date 2022-10-18 14:39:08
 */
@Data
@TableName("sys_region")
@EqualsAndHashCode(callSuper = true)
@Schema(description = "行政区域表")
public class OilRegion extends BaseEntity {

	/**
	 * 区域ID
	 */
	@TableId(type = IdType.ASSIGN_ID)
	@Schema(description = "区域ID")
	private Integer id;

	/**
	 * 上级区域ID
	 */
	@Schema(description = "上级区域ID")
	private Integer parentId;

	/**
	 * 行政区域等级 1-省 2-市 3-区县 4-街道镇
	 */
	@Schema(description = "行政区域等级 1-省 2-市 3-区县 4-街道镇")
	private Integer level;

	/**
	 * 名称
	 */
	@Schema(description = "名称")
	private String name;

	/**
	 * 完整名称
	 */
	@Schema(description = "完整名称")
	private String wholeName;

	/**
	 * 本区域经度
	 */
	@Schema(description = "本区域经度")
	private String lon;

	/**
	 * 本区域维度
	 */
	@Schema(description = "本区域维度")
	private String lat;

	/**
	 * 电话区号
	 */
	@Schema(description = "电话区号")
	private String cityCode;

	/**
	 * 邮政编码
	 */
	@Schema(description = "邮政编码")
	private String zipCode;

	/**
	 * 行政区划代码
	 */
	@Schema(description = "行政区划代码")
	private String areaCode;

	/**
	 * 名称全拼
	 */
	@Schema(description = "名称全拼")
	private String pinYin;

	/**
	 * 首字母简拼
	 */
	@Schema(description = "首字母简拼")
	private String simplePy;

	/**
	 * 区域名称拼音的第一个字母
	 */
	@Schema(description = "区域名称拼音的第一个字母")
	private String perPinYin;

}
