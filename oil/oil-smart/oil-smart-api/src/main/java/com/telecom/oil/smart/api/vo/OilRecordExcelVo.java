package com.telecom.oil.smart.api.vo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.pig4cloud.plugin.excel.annotation.ExcelLine;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 加油记录excel 对应的实体
 *
 * @author fxz
 * @date 2022/3/21
 */
@Data
@ColumnWidth(30)
public class OilRecordExcelVo implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * excel 行号
	 */
	@ExcelLine
	@ExcelIgnore
	private Long lineNum;

	/**
	 * 主键id
	 */
	@ExcelProperty("加油编号")
	private Long id;

	/**
	 * 用户id
	 */
	// @NotBlank(message = "用户id不能为空")
	// @ExcelProperty("用户id")
	// private Long userId;

	/**
	 * 用户名
	 */
	@NotBlank(message = "用户名不能为空")
	@ExcelProperty("用户名")
	private String userName;

	/**
	 * 车牌号
	 */
	@NotBlank(message = "车牌号不能为空")
	@ExcelProperty("车牌号")
	private String carNumber;

	/**
	 * 行驶里程（单位：m）
	 */
	@NotBlank(message = "行驶里程不能为空")
	@ExcelProperty("行驶里程（单位：m）")
	private BigDecimal travelMile;

	/**
	 * 加油金额（单位：元）
	 */
	@NotBlank(message = "加油金额不能为空")
	@ExcelProperty("加油金额（单位：元）")
	private BigDecimal amount;

	/**
	 * 加油站
	 */
	@NotBlank(message = "加油站不能为空")
	@ExcelProperty("加油站")
	private String stationName;

	/**
	 * 详细地址
	 */
	@NotBlank(message = "详细地址不能为空")
	@ExcelProperty("详细地址")
	private String address;

	/**
	 * 经度
	 */
	@NotBlank(message = "经度不能为空")
	@ExcelProperty("经度")
	private BigDecimal longitude;

	/**
	 * 纬度
	 */
	@NotBlank(message = "纬度不能为空")
	@ExcelProperty("纬度")
	private BigDecimal latitude;

	/**
	 * 现场照片
	 */
	// @NotBlank(message = "现场照片不能为空")
	// @ExcelProperty("现场照片")
	// private String siteImage;

	/**
	 * 收据照片
	 */
	// @NotBlank(message = "收据照片不能为空")
	// @ExcelProperty("收据照片")
	// private String receiptImage;

	/**
	 * 创建时间
	 */
	@ExcelProperty(value = "创建时间")
	private LocalDateTime createTime;

}
