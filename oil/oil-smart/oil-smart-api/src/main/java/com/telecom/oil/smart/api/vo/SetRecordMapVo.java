package com.telecom.oil.smart.api.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SetRecordMapVo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Schema(description = "主键id")
	private Long id;

	@Schema(description ="经度")
	private BigDecimal longitude;

	@Schema(description ="纬度")
	private BigDecimal latitude;

	@Schema(description = "户主")
	private String houseHolder;

	@Schema(description = "运营商")
	private String op;

}
