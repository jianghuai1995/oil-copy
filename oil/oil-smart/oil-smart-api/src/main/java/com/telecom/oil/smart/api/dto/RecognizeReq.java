package com.telecom.oil.smart.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Schema(name = "认证申请录req")
public class RecognizeReq implements Serializable {

	private static final long serialVersionID = 1L;

	@Schema(name = "用户名")
	private String userName;

	@Schema(name = "电话号码")
	private String phone;

	@Schema(name = "部门")
	private String dept;

}
