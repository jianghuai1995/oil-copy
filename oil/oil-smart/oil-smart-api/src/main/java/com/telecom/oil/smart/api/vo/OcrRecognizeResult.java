package com.telecom.oil.smart.api.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OcrRecognizeResult implements Serializable {

	private static final long serialVersionUID = 1L;

	@Schema(description = "卡号")
	private String cardNumber;

	@Schema(description = "实收金额")
	private BigDecimal receivedAmount;

	@Schema(description = "卡片余额")
	private BigDecimal cardBalance;

}
