package com.telecom.oil.smart.api.vo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.pig4cloud.plugin.excel.annotation.ExcelLine;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * C端用户excel 对应的实体
 *
 * @author fxz
 * @date 2022/3/21
 */
@Data
@ColumnWidth(30)
public class OilUserExcelVo implements Serializable {

	private static final long serialVersionUID = 1L;

	@ExcelLine
	@ExcelIgnore
	private Long lineNum;

	@ExcelProperty("用户编号")
	private Long id;

	@ExcelProperty("微信账户")
	private String weixinAccount;

	@ExcelProperty("电话号码")
	private String phone;

	// @ExcelProperty("头像")
	// private String avatarUrl;

	@ExcelProperty("用户名")
	private String userName;

	@ExcelProperty("昵称")
	private String nickName;

	@ExcelProperty("性别")
	private String sex;

	@ExcelProperty("地址")
	private String address;

	@ExcelProperty("email地址")
	private String email;

	@ExcelProperty("公司名称")
	private String company;

	@ExcelProperty("部门")
	private String deptName;

	@ExcelProperty("unionId")
	private String unionId;

	@ExcelProperty("openId")
	private String openId;

	@ExcelProperty("认证状态")
	private String recognizeStatus;

	@ExcelProperty(value = "创建时间")
	private LocalDateTime createTime;

}
