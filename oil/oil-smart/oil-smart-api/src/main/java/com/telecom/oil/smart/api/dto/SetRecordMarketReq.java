package com.telecom.oil.smart.api.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

@Data
public class SetRecordMarketReq {

	@Schema(description ="营销类别: NEW-新装; SAVE-保存; OTHER-其他 ")
	private String businessType;

	@Schema(description ="系统揽收人")
	private String receiver;

	@Schema(description ="办理日期")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date signDate;

	@Schema(description ="原套餐")
	private String oldSet;

	@Schema(description ="办理套餐")
	private String signSet;

	@Schema(description ="系统优惠")
	private String systemDiscount;

	@Schema(description ="调账金额")
	private BigDecimal adjustAmount;

	@Schema(description ="甩单编号")
	private String orderCode;

	@Schema(description ="营销积分")
	private Integer marketPoint;

	@Schema(description ="主卡号码")
	private String masterCard;

	@Schema(description ="副卡1")
	private String assistantCard1;

	@Schema(description ="副卡2")
	private String assistantCard2;

	@Schema(description ="副卡3")
	private String assistantCard3;

	@Schema(description ="副卡4")
	private String assistantCard4;

}
