package com.telecom.oil.smart.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Schema(name = "加油记录req")
public class OilRecordReq extends PageRequest implements Serializable {

	private static final long serialVersionID = 1L;

	@Schema(name = "车牌号")
	private String carNumber;

	@Schema(name = "加油站")
	private String station;

	@Schema(name = "用户ID")
	private Long userId;

}
