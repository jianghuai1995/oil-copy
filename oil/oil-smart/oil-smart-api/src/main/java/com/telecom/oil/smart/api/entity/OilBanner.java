/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.telecom.oil.smart.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.telecom.oil.common.mybatis.base.BaseEntity;
import com.telecom.oil.smart.api.enums.LockFlagEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Banner图表
 *
 * @author jianghuai
 * @date 2022-11-04 14:32:47
 */
@Data
@TableName("oil_banner")
@EqualsAndHashCode(callSuper = true)
@Schema(description = "Banner图表")
public class OilBanner extends BaseEntity {

	/**
	 * 主键id
	 */
	@TableId(type = IdType.ASSIGN_ID)
	@Schema(description = "主键id")
	private Long id;

	/**
	 * 标题
	 */
	@Schema(description = "标题")
	private String title;

	/**
	 * banner图片
	 */
	@Schema(description = "banner图片")
	private String image;

	/**
	 * banner链接
	 */
	@Schema(description = "banner链接")
	private String url;

	/**
	 * 锁定标识: 0-锁定; 1-正常
	 */
	@Schema(description = "锁定标识: 0-锁定; 1-正常")
	private Integer lockFlag;

}
