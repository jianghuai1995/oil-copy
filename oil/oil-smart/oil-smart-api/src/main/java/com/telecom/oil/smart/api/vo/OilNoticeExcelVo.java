package com.telecom.oil.smart.api.vo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.pig4cloud.plugin.excel.annotation.ExcelLine;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 通知excel 对应的实体
 *
 * @author fxz
 * @date 2022/3/21
 */
@Data
@ColumnWidth(30)
public class OilNoticeExcelVo implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * excel 行号
	 */
	@ExcelLine
	@ExcelIgnore
	private Long lineNum;

	@ExcelProperty("加油编号")
	private Long id;

	@NotBlank(message = "标题不能为空")
	@ExcelProperty("标题")
	private String title;

	@ExcelProperty("通知内容")
	private String content;

	@ExcelProperty(value = "创建时间")
	private LocalDateTime createTime;

}
