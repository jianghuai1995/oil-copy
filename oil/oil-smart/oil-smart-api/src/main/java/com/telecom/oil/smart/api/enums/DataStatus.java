package com.telecom.oil.smart.api.enums;

public enum DataStatus {

	ACTIVE(0, "正常"),
	DELETED(1, "删除");

	private Integer value;

	private String desc;

	DataStatus(Integer value, String desc) {
		this.value = value;
		this.desc = desc;
	}

	public Integer getValue() {
		return value;
	}

}