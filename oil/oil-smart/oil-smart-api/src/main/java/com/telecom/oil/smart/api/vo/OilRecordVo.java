package com.telecom.oil.smart.api.vo;

import com.telecom.oil.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OilRecordVo extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Schema(description = "主键id")
	private Long id;

	@Schema(description = "用户id")
	private Long userId;

	@Schema(description = "用户名")
	private String userName;

	@Schema(description = "用户电话")
	private String userPhone;

	@Schema(description = "车牌号")
	private String carNumber;

	@Schema(description = "行驶里程（单位：公里）")
	private BigDecimal travelMile;

	@Schema(description = "加油金额（单位：元）")
	private BigDecimal amount;

	@Schema(description = "加油站")
	private String stationName;

	@Schema(description = "详细地址")
	private String address;

	@Schema(description = "经度")
	private BigDecimal longitude;

	@Schema(description = "纬度")
	private BigDecimal latitude;

	@Schema(description = "现场照片")
	private String siteImage;

	@Schema(description = "收据照片")
	private String receiptImage;

	@Schema(description = "收据卡号")
	private String cardNumber;

	@Schema(description = "实收金额")
	private BigDecimal receivedAmount;

	@Schema(description = "卡片余额")
	private BigDecimal cardBalance;

}
