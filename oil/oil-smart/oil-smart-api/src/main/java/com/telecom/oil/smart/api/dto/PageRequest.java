package com.telecom.oil.smart.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class PageRequest {

	/**
	 * 默认页码值
	 */
	public static final long DEFAULT_CURRENT = 1;

	/**
	 * 默认分页大小
	 */
	public static final long DEFAULT_SIZE = 20;

	@Schema(name = "页码", description = "默认为1")
	private long current;

	@Schema(name = "分页大小", description = "默认为20")
	private long size;

	public PageRequest(long current, long size) {
		this.current = current;
		this.size = size;
	}

	public PageRequest() {
		this.current = DEFAULT_CURRENT;
		this.size = DEFAULT_SIZE;
	}

}
