package com.telecom.oil.smart.api.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OilRecordMapVo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Schema(description = "主键id")
	private Long id;

	@Schema(description = "车牌号")
	private String carNumber;

	@Schema(description = "经度")
	private BigDecimal longitude;

	@Schema(description = "纬度")
	private BigDecimal latitude;

}
