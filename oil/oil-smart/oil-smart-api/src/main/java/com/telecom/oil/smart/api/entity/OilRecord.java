/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.telecom.oil.smart.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.telecom.oil.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * 加油记录表
 *
 * @author jianghuai
 * @date 2022-10-18 14:39:08
 */
@Data
@TableName("oil_record")
@EqualsAndHashCode(callSuper = true)
@Schema(description = "加油记录表")
public class OilRecord extends BaseEntity {

	/**
	 * 主键id
	 */
	@TableId(type = IdType.ASSIGN_ID)
	@Schema(description = "主键id")
	private Long id;

	/**
	 * 用户id
	 */
	@Schema(description = "用户id")
	private Long userId;

	/**
	 * 车牌号
	 */
	@Schema(description = "车牌号")
	private String carNumber;

	/**
	 * 行驶里程（单位：m）
	 */
	@Schema(description = "行驶里程（单位：m）")
	private BigDecimal travelMile;

	/**
	 * 加油金额（单位：元）
	 */
	@Schema(description = "加油金额（单位：元）")
	private BigDecimal amount;

	/**
	 * 加油站
	 */
	@Schema(description = "加油站")
	private String stationName;

	/**
	 * 详细地址
	 */
	@Schema(description = "详细地址")
	private String address;

	/**
	 * 经度
	 */
	@Schema(description = "经度")
	private BigDecimal longitude;

	/**
	 * 纬度
	 */
	@Schema(description = "纬度")
	private BigDecimal latitude;

	/**
	 * 现场照片
	 */
	@Schema(description = "现场照片")
	private String siteImage;

	/**
	 * 收据照片
	 */
	@Schema(description = "收据照片")
	private String receiptImage;

	/**
	 * 收据卡号
	 */
	@Schema(description = "收据卡号")
	private String cardNumber;

	/**
	 * 实收金额
	 */
	@Schema(description = "实收金额")
	private BigDecimal receivedAmount;

	/**
	 * 卡片余额
	 */
	@Schema(description = "卡片余额")
	private BigDecimal cardBalance;

}
