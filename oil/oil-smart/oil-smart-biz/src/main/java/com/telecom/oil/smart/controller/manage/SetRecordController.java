/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.telecom.oil.smart.controller.manage;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.plugin.excel.annotation.ResponseExcel;
import com.telecom.oil.common.core.util.R;
import com.telecom.oil.common.log.annotation.SysLog;
import com.telecom.oil.smart.api.entity.SetRecord;
import com.telecom.oil.smart.api.vo.SetRecordExcelVo;
import com.telecom.oil.smart.service.SetRecordService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 套餐信息调查表
 *
 * @author jianghuai
 * @date 2023-02-07 11:55:00
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/setrecord" )
@Tag(name = "套餐信息调查表管理")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class SetRecordController {

    private final SetRecordService setRecordService;

    @Operation(summary = "分页查询", description = "分页查询")
    @GetMapping("/page" )
    @PreAuthorize("@pms.hasPermission('smart_setrecord_get')" )
    public R getSetRecordPage(Page page, SetRecord setRecord) {
        return R.ok(setRecordService.pageQuery(page, Wrappers.query(setRecord)));
    }

    @Operation(summary = "通过id查询", description = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('smart_setrecord_get')" )
    public R getById(@PathVariable("id" ) Long id) {
        return R.ok(setRecordService.getById(id));
    }

    @Operation(summary = "新增套餐信息调查表", description = "新增套餐信息调查表")
    @SysLog("新增套餐信息调查表" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('smart_setrecord_add')" )
    public R save(@RequestBody SetRecord setRecord) {
        return R.ok(setRecordService.save(setRecord));
    }

    @Operation(summary = "修改数字套餐信息调查表", description = "修改套餐信息调查表")
    @SysLog("修改套餐信息调查表" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('smart_setrecord_edit')" )
    public R updateById(@RequestBody SetRecord setRecord) {
        return R.ok(setRecordService.updateById(setRecord));
    }

    @Operation(summary = "通过id删除套餐信息调查表", description = "通过id删除套餐信息调查表")
    @SysLog("通过id删除套餐信息调查表" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('smart_setrecord_del')" )
    public R removeById(@PathVariable Long id) {
        return R.ok(setRecordService.removeById(id));
    }

	@ResponseExcel
	@Operation(summary = "导出excel 表格", description = "导出excel 表格")
	@GetMapping("/export")
	@PreAuthorize("@pms.hasPermission('smart_setrecord_export')")
	public List<SetRecordExcelVo> export() {
		return setRecordService.listRecord();
	}

}
