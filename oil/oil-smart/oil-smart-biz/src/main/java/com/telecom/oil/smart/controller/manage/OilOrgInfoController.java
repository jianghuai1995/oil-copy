/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.telecom.oil.smart.controller.manage;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.telecom.oil.common.core.util.R;
import com.telecom.oil.common.log.annotation.SysLog;
import com.telecom.oil.smart.api.entity.OilOrgInfo;
import com.telecom.oil.smart.service.OilOrgInfoService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

/**
 * 用户组织表
 *
 * @author jianghuai
 * @date 2022-10-18 14:39:08
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/oilorginfo")
@Tag(name = "用户组织表管理")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class OilOrgInfoController {

	private final OilOrgInfoService oilOrgInfoService;

	@Operation(summary = "分页查询", description = "分页查询")
	@GetMapping("/page")
	@PreAuthorize("@pms.hasPermission('smart_oilorginfo_get')")
	public R getOrgInfoPage(Page page, OilOrgInfo oilOrgInfo) {
		return R.ok(oilOrgInfoService.page(page, Wrappers.query(oilOrgInfo)));
	}

	@Operation(summary = "通过id查询", description = "通过id查询")
	@GetMapping("/{id}")
	@PreAuthorize("@pms.hasPermission('smart_oilorginfo_get')")
	public R getById(@PathVariable("id") Integer id) {
		return R.ok(oilOrgInfoService.getById(id));
	}

	@Operation(summary = "新增用户组织", description = "新增用户组织表")
	@SysLog("新增用户组织表")
	@PostMapping
	@PreAuthorize("@pms.hasPermission('smart_oilorginfo_add')")
	public R save(@RequestBody OilOrgInfo oilOrgInfo) {
		return R.ok(oilOrgInfoService.save(oilOrgInfo));
	}

	@Operation(summary = "修改用户组织", description = "修改用户组织表")
	@SysLog("修改用户组织表")
	@PutMapping
	@PreAuthorize("@pms.hasPermission('smart_oilorginfo_edit')")
	public R updateById(@RequestBody OilOrgInfo oilOrgInfo) {
		return R.ok(oilOrgInfoService.updateById(oilOrgInfo));
	}

	@Operation(summary = "通过id删除用户组织", description = "通过id删除用户组织表")
	@SysLog("通过id删除用户组织表")
	@DeleteMapping("/{id}")
	@PreAuthorize("@pms.hasPermission('smart_oilorginfo_del')")
	public R removeById(@PathVariable Integer id) {
		return R.ok(oilOrgInfoService.removeById(id));
	}

}
