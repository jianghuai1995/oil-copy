package com.telecom.oil.smart.controller.mini;

import com.telecom.oil.common.core.util.R;
import com.telecom.oil.common.security.annotation.Inner;
import com.telecom.oil.smart.service.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mini/banner")
@Tag(name = "微信小程序端-banner模块")
@Inner(value = false)
@RequiredArgsConstructor
public class MiniOilBannerController {

	private final OilBannerService oilBannerService;

	@Operation(summary = "banner图列表")
	@GetMapping("/list")
	public R bannerCList() {
		return R.ok(oilBannerService.bannerCList());
	}

}
