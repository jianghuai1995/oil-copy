/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.telecom.oil.smart.controller.manage;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.telecom.oil.common.core.util.R;
import com.telecom.oil.common.log.annotation.SysLog;
import com.telecom.oil.smart.api.entity.OilRegion;
import com.telecom.oil.smart.service.OilRegionService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

/**
 * 行政区域表
 *
 * @author jianghuai
 * @date 2022-10-18 14:39:08
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/oilregion")
@Tag(name = "行政区域表管理")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class OilRegionController {

	private final OilRegionService oilRegionService;

	@Operation(summary = "分页查询", description = "分页查询")
	@GetMapping("/page")
	// @PreAuthorize("@pms.hasPermission('smart_sysregion_get')")
	public R getSysRegionPage(Page page, OilRegion oilRegion) {
		return R.ok(oilRegionService.page(page, Wrappers.query(oilRegion)));
	}

	@Operation(summary = "通过id查询", description = "通过id查询")
	@GetMapping("/{id}")
	@PreAuthorize("@pms.hasPermission('smart_sysregion_get')")
	public R getById(@PathVariable("id") Integer id) {
		return R.ok(oilRegionService.getById(id));
	}

	@Operation(summary = "新增行政区域", description = "新增行政区域表")
	@SysLog("新增行政区域表")
	@PostMapping
	@PreAuthorize("@pms.hasPermission('smart_sysregion_add')")
	public R save(@RequestBody OilRegion oilRegion) {
		return R.ok(oilRegionService.save(oilRegion));
	}

	@Operation(summary = "修改行政区域", description = "修改行政区域表")
	@SysLog("修改行政区域表")
	@PutMapping
	@PreAuthorize("@pms.hasPermission('smart_sysregion_edit')")
	public R updateById(@RequestBody OilRegion oilRegion) {
		return R.ok(oilRegionService.updateById(oilRegion));
	}

	@Operation(summary = "通过id删除行政区域", description = "通过id删除行政区域表")
	@SysLog("通过id删除行政区域表")
	@DeleteMapping("/{id}")
	@PreAuthorize("@pms.hasPermission('smart_sysregion_del')")
	public R removeById(@PathVariable Integer id) {
		return R.ok(oilRegionService.removeById(id));
	}

}
