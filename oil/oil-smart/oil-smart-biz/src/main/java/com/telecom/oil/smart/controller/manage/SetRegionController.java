/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.telecom.oil.smart.controller.manage;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.telecom.oil.common.core.util.R;
import com.telecom.oil.common.log.annotation.SysLog;
import com.telecom.oil.smart.api.entity.SetRegion;
import com.telecom.oil.smart.service.SetRegionService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;


/**
 * 行政区域表
 *
 * @author jianghuai
 * @date 2023-02-07 11:47:42
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/setregion" )
@Tag(name = "行政区域表管理")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class SetRegionController {

    private final SetRegionService setRegionService;

    @Operation(summary = "分页查询", description = "分页查询")
    @GetMapping("/page" )
    @PreAuthorize("@pms.hasPermission('smart_setregion_get')" )
    public R getSetRegionPage(Page page, SetRegion setRegion) {
        return R.ok(setRegionService.page(page, Wrappers.query(setRegion)));
    }

    @Operation(summary = "通过id查询", description = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('smart_setregion_get')" )
    public R getById(@PathVariable("id" ) Integer id) {
        return R.ok(setRegionService.getById(id));
    }

    @Operation(summary = "新增行政区域表", description = "新增行政区域表")
    @SysLog("新增行政区域表" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('smart_setregion_add')" )
    public R save(@RequestBody SetRegion setRegion) {
        return R.ok(setRegionService.save(setRegion));
    }

    @Operation(summary = "修改行政区域表", description = "修改行政区域表")
    @SysLog("修改行政区域表" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('smart_setregion_edit')" )
    public R updateById(@RequestBody SetRegion setRegion) {
        return R.ok(setRegionService.updateById(setRegion));
    }

    @Operation(summary = "通过id删除行政区域表", description = "通过id删除行政区域表")
    @SysLog("通过id删除行政区域表" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('smart_setregion_del')" )
    public R removeById(@PathVariable Integer id) {
        return R.ok(setRegionService.removeById(id));
    }

}
