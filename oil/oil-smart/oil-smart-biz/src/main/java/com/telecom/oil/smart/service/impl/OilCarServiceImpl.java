/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.telecom.oil.smart.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.telecom.oil.common.core.constant.CacheConstants;
import com.telecom.oil.smart.api.entity.OilCar;
import com.telecom.oil.smart.mapper.OilCarMapper;
import com.telecom.oil.smart.service.OilCarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 
 *
 * @author jianghuai
 * @date 2023-05-06 15:16:46
 */
@Service
public class OilCarServiceImpl extends ServiceImpl<OilCarMapper, OilCar> implements OilCarService {

	@Autowired
	private OilCarMapper oilCarMapper;

	@Override
	@Cacheable(value = CacheConstants.CAR_NUMBER_DETAILS, unless = "#result == null")
	public List<String> listAllCarNumber() {
		return Optional.ofNullable(oilCarMapper.selectAllCarNumber())
				.orElse(new ArrayList<>(0))
				.stream()
				.distinct()
				.collect(Collectors.toList());
	}
}
