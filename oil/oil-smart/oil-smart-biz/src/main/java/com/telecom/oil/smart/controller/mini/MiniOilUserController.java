package com.telecom.oil.smart.controller.mini;

import com.telecom.oil.common.core.util.R;
import com.telecom.oil.common.security.annotation.Inner;
import com.telecom.oil.smart.api.entity.OilUser;
import com.telecom.oil.smart.service.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/mini/userinfo")
@Tag(name = "微信小程序端-用户信息模块")
@Inner(value = false)
@RequiredArgsConstructor
public class MiniOilUserController extends MiniBaseController {

	private final OilUserService oilUserService;

	@Operation(summary = "微信一键登录", description = "微信一键登录")
	@GetMapping("/{code}")
	public R weixinLogin(@PathVariable("code") String code) {
		return R.ok(oilUserService.weixinLogin(code));
	}

	@Operation(summary = "申请认证", description = "申请认证")
	@GetMapping("/identify")
	public R identifyApply(HttpServletRequest request) {
		com.telecom.oil.common.security.service.OilUser user = checkUserWithLoginRight(request);
		oilUserService.identifyApply(user.getId());
		return R.ok();
	}

	@Operation(summary = "获取个人信息", description = "获取个人信息")
	@GetMapping("/info")
	public R getUserInfo(HttpServletRequest request) {
		com.telecom.oil.common.security.service.OilUser user = checkUserWithLoginRight(request);
		return R.ok(oilUserService.getUserInfo(user.getId()));
	}

	@Operation(summary = "修改个人信息", description = "修改个人信息")
	@PostMapping("/update")
	public R updateUserInfo(HttpServletRequest request, @RequestBody OilUser oilUser) {
		com.telecom.oil.common.security.service.OilUser user = checkUserWithLoginRight(request);
		oilUser.setId(user.getId());
		oilUserService.updateById(oilUser);
		return R.ok();
	}

}
