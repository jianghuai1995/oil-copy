/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.telecom.oil.smart.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.telecom.oil.smart.api.entity.SetRecord;
import com.telecom.oil.smart.api.vo.SetRecordVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 套餐信息调查表
 *
 * @author jianghuai
 * @date 2023-02-07 11:55:00
 */
@Mapper
public interface SetRecordMapper extends BaseMapper<SetRecord> {

    List<SetRecord> listSetRecordMap();

	SetRecordVo selectSetRecordDetailById(Long id);

}
