/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.telecom.oil.smart.controller.manage;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.plugin.excel.annotation.ResponseExcel;
import com.telecom.oil.common.core.util.R;
import com.telecom.oil.common.log.annotation.SysLog;
import com.telecom.oil.smart.api.entity.OilNotice;
import com.telecom.oil.smart.api.vo.OilNoticeExcelVo;
import com.telecom.oil.smart.service.OilNoticeService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 通知表
 *
 * @author jianghuai
 * @date 2022-10-18 14:39:08
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/oilnotice")
@Tag(name = "通知表管理")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class OilNoticeController {

	private final OilNoticeService oilNoticeService;

	@Operation(summary = "分页查询", description = "分页查询")
	@GetMapping("/page")
	@PreAuthorize("@pms.hasPermission('smart_oilnotice_get')")
	public R getOilNoticePage(Page page, OilNotice oilNotice) {
		return R.ok(oilNoticeService.page(page, Wrappers.query(oilNotice)));
	}

	@Operation(summary = "通过id查询", description = "通过id查询")
	@GetMapping("/{id}")
	@PreAuthorize("@pms.hasPermission('smart_oilnotice_get')")
	public R getById(@PathVariable("id") Long id) {
		return R.ok(oilNoticeService.getById(id));
	}

	@Operation(summary = "新增通知", description = "新增通知表")
	@SysLog("新增通知表")
	@PostMapping
	@PreAuthorize("@pms.hasPermission('smart_oilnotice_add')")
	public R save(@RequestBody OilNotice oilNotice) {
		return R.ok(oilNoticeService.save(oilNotice));
	}

	@Operation(summary = "修改通知", description = "修改通知表")
	@SysLog("修改通知表")
	@PutMapping
	@PreAuthorize("@pms.hasPermission('smart_oilnotice_edit')")
	public R updateById(@RequestBody OilNotice oilNotice) {
		return R.ok(oilNoticeService.updateById(oilNotice));
	}

	@Operation(summary = "通过id删除通知", description = "通过id删除通知表")
	@SysLog("通过id删除通知表")
	@DeleteMapping("/{id}")
	@PreAuthorize("@pms.hasPermission('smart_oilnotice_del')")
	public R removeById(@PathVariable Long id) {
		return R.ok(oilNoticeService.removeById(id));
	}

	@ResponseExcel
	@Operation(summary = "导出excel 表格", description = "导出excel 表格")
	@GetMapping("/export")
	@PreAuthorize("@pms.hasPermission('smart_oilnotice_export')")
	public List<OilNoticeExcelVo> export() {
		return oilNoticeService.listNotice();
	}

}
