/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.telecom.oil.smart.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.telecom.oil.smart.api.dto.OilNoticeReq;
import com.telecom.oil.smart.api.entity.OilNotice;
import com.telecom.oil.smart.api.util.PageUtils;
import com.telecom.oil.smart.api.vo.OilNoticeExcelVo;
import com.telecom.oil.smart.mapper.OilNoticeMapper;
import com.telecom.oil.smart.service.OilNoticeService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 通知表
 *
 * @author jianghuai
 * @date 2022-10-18 14:39:08
 */
@Slf4j
@Service
@AllArgsConstructor
public class OilNoticeServiceImpl extends ServiceImpl<OilNoticeMapper, OilNotice> implements OilNoticeService {

	@Override
	public PageUtils getNoticePage(OilNoticeReq req) {
		LambdaQueryWrapper<OilNotice> lambdaQuery = Wrappers.lambdaQuery();
		lambdaQuery.like(StringUtils.isNotEmpty(req.getTitle()), OilNotice::getTitle, req.getTitle());
		lambdaQuery.orderByDesc(OilNotice::getCreateTime);
		Page<OilNotice> page = this.page(new Page<>(req.getCurrent(), req.getSize()), lambdaQuery);
		List<OilNotice> voList = page.getRecords().stream().sorted(new Comparator<OilNotice>() {
			@Override
			public int compare(OilNotice o1, OilNotice o2) {
				return -(o1.getCreateTime().compareTo(o2.getCreateTime()));
			}
		}).collect(Collectors.toList());
		return new PageUtils(voList, (int) page.getTotal(), (int) page.getSize(), (int) page.getCurrent());
	}

	@Override
	public List<OilNoticeExcelVo> listNotice() {
		List<OilNotice> noticeList = this.list(Wrappers.emptyWrapper());
		// 转换成execl 对象输出
		return noticeList.stream().map(notice -> {
			OilNoticeExcelVo oilNoticeExcelVo = new OilNoticeExcelVo();
			BeanUtil.copyProperties(notice, oilNoticeExcelVo);
			return oilNoticeExcelVo;
		}).collect(Collectors.toList());
	}

}
