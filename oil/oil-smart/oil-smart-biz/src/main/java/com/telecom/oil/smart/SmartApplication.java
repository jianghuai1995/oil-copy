package com.telecom.oil.smart;

import com.telecom.oil.common.feign.annotation.EnableOilFeignClients;
import com.telecom.oil.common.security.annotation.EnableOilResourceServer;
import com.telecom.oil.common.swagger.annotation.EnableOilDoc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author pig archetype
 * <p>
 * 项目启动类
 */
@EnableOilDoc
@EnableOilFeignClients
@EnableOilResourceServer
@EnableDiscoveryClient
@SpringBootApplication
public class SmartApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmartApplication.class, args);
	}

}
