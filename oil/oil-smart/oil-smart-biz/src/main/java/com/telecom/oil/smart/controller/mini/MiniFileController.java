package com.telecom.oil.smart.controller.mini;

import com.telecom.oil.common.core.util.R;
import com.telecom.oil.common.security.annotation.Inner;
import com.telecom.oil.smart.service.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/mini/file")
@Tag(name = "微信小程序端-文件管理模块")
@Inner(value = false)
@RequiredArgsConstructor
public class MiniFileController {

	private final OilFileService oilFileService;

	@Operation(summary = "移动端文件上传")
	@PostMapping(value = "/upload")
	public R uploadImg(@RequestPart("file") MultipartFile file) {
		return oilFileService.uploadFile(file);
	}

}
