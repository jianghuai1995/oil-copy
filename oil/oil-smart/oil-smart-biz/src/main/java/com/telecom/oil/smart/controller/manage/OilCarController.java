/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.telecom.oil.smart.controller.manage;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.telecom.oil.common.core.util.R;
import com.telecom.oil.common.log.annotation.SysLog;
import com.telecom.oil.smart.api.entity.OilCar;
import com.telecom.oil.smart.service.OilCarService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


/**
 * 
 *
 * @author jianghuai
 * @date 2023-05-06 15:16:46
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/oilcar" )
@Tag(name = "管理")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class OilCarController {

    private final OilCarService oilCarService;

    @Operation(summary = "分页查询", description = "分页查询")
    @GetMapping("/page" )
    @PreAuthorize("@pms.hasPermission('smart_oilcar_get')" )
    public R getOilCarPage(Page page, OilCar oilCar) {
        return R.ok(oilCarService.page(page, Wrappers.query(oilCar)));
    }

    @Operation(summary = "通过id查询", description = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('smart_oilcar_get')" )
    public R getById(@PathVariable("id" ) Long id) {
        return R.ok(oilCarService.getById(id));
    }

    @Operation(summary = "新增", description = "新增")
    @SysLog("新增" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('smart_oilcar_add')" )
    public R save(@RequestBody OilCar oilCar) {
        return R.ok(oilCarService.save(oilCar));
    }

    @Operation(summary = "修改", description = "修改")
    @SysLog("修改" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('smart_oilcar_edit')" )
    public R updateById(@RequestBody OilCar oilCar) {
        return R.ok(oilCarService.updateById(oilCar));
    }

    @Operation(summary = "通过id删除", description = "通过id删除")
    @SysLog("通过id删除" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('smart_oilcar_del')" )
    public R removeById(@PathVariable Long id) {
        return R.ok(oilCarService.removeById(id));
    }

	@Operation(summary = "车牌号列表", description = "车牌号列表")
	@SysLog("车牌号列表" )
	@GetMapping("/list")
	public R listOilCarNumber() {
		return R.ok(oilCarService.listAllCarNumber());
	}

}
