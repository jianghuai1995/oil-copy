package com.telecom.oil.smart.controller.mini;

import com.telecom.oil.common.core.exception.BusinessException;
import com.telecom.oil.common.core.util.R;
import com.telecom.oil.common.security.annotation.Inner;
import com.telecom.oil.smart.api.dto.OilRecordReq;
import com.telecom.oil.smart.api.entity.OilRecord;
import com.telecom.oil.smart.api.entity.OilUser;
import com.telecom.oil.smart.api.enums.RecognizeStatusEnum;
import com.telecom.oil.smart.service.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/mini/oilrecord")
@Tag(name = "微信小程序端-加油记录模块")
@Inner(value = false)
@RequiredArgsConstructor
public class MiniOilRecordController extends MiniBaseController {

	private final OilRecordService oilRecordService;
	private final OilUserService oilUserService;

	@Operation(summary = "分页查询加油记录", description = "分页查询加油记录")
	@GetMapping("/page")
	public R getOilRecordPage(HttpServletRequest request, OilRecordReq req) {
		checkUserWithFullRight(request);
		return R.ok(oilRecordService.getOilRecordPage(req));
	}

	@Operation(summary = "通过id查询加油记录详情", description = "通过id查询加油记录详情")
	@GetMapping("/{id}")
	public R getOilRecordById(HttpServletRequest request, @PathVariable("id") Long id) {
		checkUserWithFullRight(request);
		return R.ok(oilRecordService.getOilRecordById(id));
	}

	@Operation(summary = "上报加油", description = "上报加油")
	@PostMapping("/add")
	public R upLoadOilRecord(HttpServletRequest request, @RequestBody OilRecord oilRecord) {
		com.telecom.oil.common.security.service.OilUser user = checkUserWithFullRight(request);
		oilRecord.setUserId(user.getId());
		oilRecord.setCreateBy(user.getUsername());
		return R.ok(oilRecordService.upLoadOilRecord(oilRecord));
	}

	@Operation(summary = "我的加油记录", description = "我的加油记录")
	@GetMapping("/myoilrecord")
	public R findMyOilRecord(HttpServletRequest request, OilRecordReq req) {
		com.telecom.oil.common.security.service.OilUser user = checkUserWithFullRight(request);
		// 实时判定用户认证状态
		OilUser userInfo = oilUserService.getById(user.getId());
		if (!userInfo.getRecognizeStatus().equals(RecognizeStatusEnum.RECOGNIZED.getCode())) {
			throw new BusinessException("请先认证");
		}
		req.setUserId(user.getId());
		return R.ok(oilRecordService.findMyOilRecord(req));
	}

	@Operation(summary = "更新加油记录", description = "更新加油记录")
	@PutMapping("/update")
	public R updateById(HttpServletRequest request, @RequestBody OilRecord oilRecord) {
		checkUserWithFullRight(request);
		return R.ok(oilRecordService.updateById(oilRecord));
	}

	@Operation(summary = "地图展示加油记录", description = "地图展示加油记录")
	@GetMapping("/map/list")
	public R listAllOilRecord(HttpServletRequest request) {
		checkUserWithFullRight(request);
		return R.ok(oilRecordService.listOilRecordMap());
	}

}
