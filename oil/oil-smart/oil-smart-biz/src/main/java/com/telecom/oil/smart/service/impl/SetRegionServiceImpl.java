/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.telecom.oil.smart.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.telecom.oil.common.core.constant.CacheConstants;
import com.telecom.oil.smart.api.entity.SetRegion;
import com.telecom.oil.smart.api.vo.SetRegionVo;
import com.telecom.oil.smart.mapper.SetRegionMapper;
import com.telecom.oil.smart.service.SetRegionService;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 行政区域表
 *
 * @author jianghuai
 * @date 2023-02-07 11:47:42
 */
@Service
public class SetRegionServiceImpl extends ServiceImpl<SetRegionMapper, SetRegion> implements SetRegionService {

	@Override
	@Cacheable(value = CacheConstants.REGION_DETAILS, key = "T(String).valueOf(#level).concat('-').concat(#parentId)", unless = "#result == null")
	public List<SetRegionVo> getRegionListByLevelAndParentId(Integer level, Integer parentId) {
		LambdaQueryWrapper<SetRegion> wrapper = Wrappers.lambdaQuery();
		wrapper.eq(SetRegion::getParentId, parentId);
		wrapper.eq(SetRegion::getLevel, level);
		wrapper.orderByAsc(SetRegion::getId);
		List<SetRegion> list = this.list(wrapper);
		List<SetRegionVo> voList = list.stream().map(item -> {
			SetRegionVo vo = new SetRegionVo();
			vo.setId(item.getId());
			vo.setName(item.getName());
			return vo;
		}).collect(Collectors.toList());
		return voList;
	}

	@Override
	@Cacheable(value = CacheConstants.REGION_DETAILS, key = "#id", unless = "#result == null")
	public SetRegion findById(Integer id) {
		return this.getById(id);
	}

}
