/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.telecom.oil.smart.controller.manage;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.telecom.oil.common.core.util.R;
import com.telecom.oil.common.log.annotation.SysLog;
import com.telecom.oil.common.security.annotation.Inner;
import com.telecom.oil.smart.api.entity.OilFile;
import com.telecom.oil.smart.service.OilFileService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpHeaders;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * 文件管理
 *
 * @author Luckly
 * @date 2021-09-11
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/oilfile")
@Tag(name = "文件管理模块")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class OilFileController {

	private final OilFileService oilFileService;

	@Operation(summary = "分页查询", description = "分页查询")
	@GetMapping("/page")
	public R<IPage<OilFile>> getSysFilePage(Page page, OilFile oilFile) {
		return R.ok(oilFileService.page(page, Wrappers.<OilFile>lambdaQuery()
				.like(StrUtil.isNotBlank(oilFile.getFileName()), OilFile::getFileName, oilFile.getFileName())));
	}

	@Operation(summary = "通过id删除文件管理", description = "通过id删除文件管理")
	@SysLog("删除文件管理")
	@DeleteMapping("/{id:\\d+}")
	@PreAuthorize("@pms.hasPermission('oil_file_del')")
	public R<Boolean> removeById(@PathVariable Long id) {
		return R.ok(oilFileService.deleteFile(id));
	}

	@Operation(summary = "管理后台文件上传")
	@PostMapping(value = "/upload")
	public R upload(@RequestPart("file") MultipartFile file) {
		return oilFileService.uploadFile(file);
	}

	@Inner(false)
	@GetMapping("/{bucket}/{fileName}")
	public void file(@PathVariable String bucket, @PathVariable String fileName, HttpServletResponse response) {
		oilFileService.getFile(bucket, fileName, response);
	}

	@SneakyThrows
	@GetMapping("/local/{fileName}")
	public void localFile(@PathVariable String fileName, HttpServletResponse response) {
		ClassPathResource resource = new ClassPathResource("file/" + fileName);
		response.setContentType("application/octet-stream; charset=UTF-8");
		IoUtil.copy(resource.getInputStream(), response.getOutputStream());
	}

	@Inner(false)
	@GetMapping("/online/{bucket}/{fileName}")
	public R<String> onlineFile(@PathVariable String bucket, @PathVariable String fileName) {
		return R.ok(oilFileService.onlineFile(bucket, fileName));
	}

}
