/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.telecom.oil.smart.controller.manage;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.telecom.oil.common.core.util.R;
import com.telecom.oil.common.log.annotation.SysLog;
import com.telecom.oil.smart.api.entity.SetRecordMarket;
import com.telecom.oil.smart.service.SetRecordMarketService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;


/**
 * 营销套餐表
 *
 * @author jianghuai
 * @date 2023-05-01 21:13:52
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/setrecordmarket" )
@Tag(name = "营销套餐表管理")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class SetRecordMarketController {

    private final  SetRecordMarketService setRecordMarketService;

    @Operation(summary = "分页查询", description = "分页查询")
    @GetMapping("/page" )
    @PreAuthorize("@pms.hasPermission('smart_setrecordmarket_get')" )
    public R getSetRecordMarketPage(Page page, SetRecordMarket setRecordMarket) {
        return R.ok(setRecordMarketService.page(page, Wrappers.query(setRecordMarket)));
    }

    @Operation(summary = "通过id查询", description = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('smart_setrecordmarket_get')" )
    public R getById(@PathVariable("id" ) Long id) {
        return R.ok(setRecordMarketService.getById(id));
    }

    @Operation(summary = "新增营销套餐表", description = "新增营销套餐表")
    @SysLog("新增营销套餐表" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('smart_setrecordmarket_add')" )
    public R save(@RequestBody SetRecordMarket setRecordMarket) {
        return R.ok(setRecordMarketService.save(setRecordMarket));
    }

    @Operation(summary = "修改营销套餐表", description = "修改营销套餐表")
    @SysLog("修改营销套餐表" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('smart_setrecordmarket_edit')" )
    public R updateById(@RequestBody SetRecordMarket setRecordMarket) {
        return R.ok(setRecordMarketService.updateById(setRecordMarket));
    }

    @Operation(summary = "通过id删除营销套餐表", description = "通过id删除营销套餐表")
    @SysLog("通过id删除营销套餐表" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('smart_setrecordmarket_del')" )
    public R removeById(@PathVariable Long id) {
        return R.ok(setRecordMarketService.removeById(id));
    }

}
