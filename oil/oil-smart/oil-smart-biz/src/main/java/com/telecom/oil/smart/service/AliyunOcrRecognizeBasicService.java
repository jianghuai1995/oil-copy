package com.telecom.oil.smart.service;

import com.telecom.oil.smart.api.vo.OcrRecognizeResult;

public interface AliyunOcrRecognizeBasicService {

	OcrRecognizeResult recognizeBasic(String url);

}
