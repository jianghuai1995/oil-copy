package com.telecom.oil.smart.controller.mini;

import com.telecom.oil.common.core.util.R;
import com.telecom.oil.common.security.annotation.Inner;
import com.telecom.oil.smart.service.OilCarService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/mini/oilcar")
@Tag(name = "微信小程序端-车辆信息模块")
@Inner(value = false)
@RequiredArgsConstructor
public class MiniOilCarController extends MiniBaseController {

	private final OilCarService oilCarService;

	@Operation(summary = "车牌号列表", description = "车牌号列表")
	@GetMapping("/list")
	public R listOilCarNumber(HttpServletRequest request) {
		checkUserWithFullRight(request);
		return R.ok(oilCarService.listAllCarNumber());
	}

}
