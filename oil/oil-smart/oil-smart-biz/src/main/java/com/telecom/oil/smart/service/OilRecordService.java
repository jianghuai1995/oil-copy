/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.telecom.oil.smart.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.telecom.oil.smart.api.dto.OilRecordReq;
import com.telecom.oil.smart.api.entity.OilRecord;
import com.telecom.oil.smart.api.util.PageUtils;
import com.telecom.oil.smart.api.vo.OilRecordExcelVo;
import com.telecom.oil.smart.api.vo.OilRecordMapVo;
import com.telecom.oil.smart.api.vo.OilRecordVo;

import java.util.List;

/**
 * 加油记录表
 *
 * @author jianghuai
 * @date 2022-10-18 14:39:08
 */
public interface OilRecordService extends IService<OilRecord> {

	/**
	 * 微信小程序端-分页查询加油记录
	 */
	PageUtils getOilRecordPage(OilRecordReq req);

	/**
	 * 微信小程序端-通过id查询加油记录详情
	 */
	OilRecordVo getOilRecordDetailCById(Long id);

	/**
	 * 微信小程序端-上报加油
	 */
	Boolean upLoadOilRecord(OilRecord oilRecord);

	/**
	 * 微信小程序端-我的加油记录
	 */
	PageUtils findMyOilRecord(OilRecordReq req);

	/**
	 * 微信小程序端-地图应用展示加油记录
	 */
	List<OilRecordMapVo> listOilRecordMap();



	OilRecordVo getOilRecordById(Long id);

	List<OilRecordExcelVo> listRecord();

    IPage pageQuery(Page page, QueryWrapper<OilRecord> query);


}
