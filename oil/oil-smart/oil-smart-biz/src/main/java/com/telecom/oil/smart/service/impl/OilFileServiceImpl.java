/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.telecom.oil.smart.service.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.amazonaws.services.s3.model.S3Object;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.plugin.oss.OssProperties;
import com.pig4cloud.plugin.oss.service.OssTemplate;
import com.telecom.oil.common.core.util.R;
import com.telecom.oil.smart.api.entity.OilFile;
import com.telecom.oil.smart.mapper.OilFileMapper;
import com.telecom.oil.smart.service.OilFileService;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 文件管理
 *
 */
@Slf4j
@Service
@AllArgsConstructor
public class OilFileServiceImpl extends ServiceImpl<OilFileMapper, OilFile> implements OilFileService {

	private final OssProperties ossProperties;

	private final OssTemplate ossTemplate;

	/**
	 * 文件上传
	 * @param file
	 * @return
	 */
	@Override
	public R uploadFile(MultipartFile file) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String dateStr = sdf.format(new Date());
		String fileName = IdUtil.simpleUUID() + StrUtil.DOT + FileUtil.extName(file.getOriginalFilename());
		Map<String, String> resultMap = new HashMap<>(4);
		resultMap.put("bucketName", ossProperties.getBucketName());
		resultMap.put("fileName", fileName);
		resultMap.put("url",
				String.format("%s/%s/%s/%s", ossProperties.getCustomDomain(),
						ossProperties.getBucketName(),
						dateStr.substring(0, 8),
						fileName));
		try {
			ossTemplate.putObject(ossProperties.getBucketName() + "/" + dateStr.substring(0, 8), fileName,
					file.getContentType(), file.getInputStream());
			// 文件管理数据记录, 收集管理追踪文件
			fileLog(file, fileName);
		}
		catch (Exception e) {
			log.error("上传失败", e);
			return R.failed(e.getLocalizedMessage());
		}
		return R.ok(resultMap);
	}

	@Override
	public void getFile(String bucket, String fileName, HttpServletResponse response) {
		try (S3Object s3Object = ossTemplate.getObject(bucket, fileName)) {
			response.setContentType("application/octet-stream; charset=UTF-8");
			IoUtil.copy(s3Object.getObjectContent(), response.getOutputStream());
		}
		catch (Exception e) {
			log.error("文件读取异常: {}", e.getLocalizedMessage());
		}
	}

	@Override
	@SneakyThrows
	@Transactional(rollbackFor = Exception.class)
	public Boolean deleteFile(Long id) {
		OilFile file = this.getById(id);
		ossTemplate.removeObject(ossProperties.getBucketName(), file.getFileName());
		return this.removeById(id);
	}

	private void fileLog(MultipartFile file, String fileName) {
		OilFile oilFile = new OilFile();
		oilFile.setFileName(fileName);
		oilFile.setOriginal(file.getOriginalFilename());
		oilFile.setFileSize(file.getSize());
		oilFile.setType(FileUtil.extName(file.getOriginalFilename()));
		oilFile.setBucketName(ossProperties.getBucketName());
		this.save(oilFile);
	}

	@Override
	public String onlineFile(String bucket, String fileName) {
		return ossTemplate.getObjectURL(bucket, fileName, Duration.of(7, ChronoUnit.DAYS));
	}

}
