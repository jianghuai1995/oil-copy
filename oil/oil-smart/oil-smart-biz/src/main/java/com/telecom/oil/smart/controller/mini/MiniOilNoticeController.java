package com.telecom.oil.smart.controller.mini;

import com.telecom.oil.common.core.util.R;
import com.telecom.oil.common.security.annotation.Inner;
import com.telecom.oil.common.security.service.OilUser;
import com.telecom.oil.smart.api.dto.OilNoticeReq;
import com.telecom.oil.smart.api.enums.RecognizeStatusEnum;
import com.telecom.oil.smart.service.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/mini/notice")
@Tag(name = "微信小程序端-通知模块")
@Inner(value = false)
@RequiredArgsConstructor
public class MiniOilNoticeController extends MiniBaseController {

	private final OilNoticeService oilNoticeService;

	@Operation(summary = "分页查询通知", description = "分页查询通知")
	@GetMapping("/page")
	public R getNoticePage(HttpServletRequest request, OilNoticeReq req) {
		checkUserWithFullRight(request);
		return R.ok(oilNoticeService.getNoticePage(req));
	}

	@Operation(summary = "通过id查询通知详情", description = "通过id查询通知详情")
	@GetMapping("/{id}")
	public R getById(HttpServletRequest request, @PathVariable("id") Long id) {
		checkUserWithFullRight(request);
		return R.ok(oilNoticeService.getById(id));
	}

}
