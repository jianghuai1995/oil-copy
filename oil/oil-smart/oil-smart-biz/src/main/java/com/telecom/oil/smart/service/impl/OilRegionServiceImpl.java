/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.telecom.oil.smart.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.telecom.oil.smart.api.entity.OilRegion;
import com.telecom.oil.smart.mapper.OilRegionMapper;
import com.telecom.oil.smart.service.OilRegionService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 行政区域表
 *
 * @author jianghuai
 * @date 2022-10-18 14:39:08
 */
@Slf4j
@Service
@AllArgsConstructor
public class OilRegionServiceImpl extends ServiceImpl<OilRegionMapper, OilRegion> implements OilRegionService {

}
