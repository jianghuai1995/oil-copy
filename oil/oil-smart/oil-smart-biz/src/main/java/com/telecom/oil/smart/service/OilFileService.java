/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.telecom.oil.smart.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.telecom.oil.common.core.util.R;
import com.telecom.oil.smart.api.entity.OilFile;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * 文件管理
 *
 */
public interface OilFileService extends IService<OilFile> {

	R uploadFile(MultipartFile file);

	void getFile(String bucket, String fileName, HttpServletResponse response);

	Boolean deleteFile(Long id);

	String onlineFile(String bucket, String fileName);

}
