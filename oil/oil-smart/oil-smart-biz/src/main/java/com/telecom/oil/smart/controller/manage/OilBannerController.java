/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.telecom.oil.smart.controller.manage;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.telecom.oil.common.core.util.R;
import com.telecom.oil.common.log.annotation.SysLog;
import com.telecom.oil.smart.api.dto.OilBannerBatchDelReq;
import com.telecom.oil.smart.api.entity.OilBanner;
import com.telecom.oil.smart.service.OilBannerService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

/**
 * Banner图表
 *
 * @author jianghuai
 * @date 2022-11-04 14:32:47
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/oilbanner")
@Tag(name = "Banner图表管理")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class OilBannerController {

	private final OilBannerService oilBannerService;

	@Operation(summary = "分页查询", description = "分页查询")
	@GetMapping("/page")
	@PreAuthorize("@pms.hasPermission('smart_oilbanner_get')")
	public R getOilBannerPage(Page page, OilBanner oilBanner) {
		return R.ok(oilBannerService.page(page, Wrappers.query(oilBanner)));
	}

	@Operation(summary = "通过id查询", description = "通过id查询")
	@GetMapping("/{id}")
	@PreAuthorize("@pms.hasPermission('smart_oilbanner_get')")
	public R getById(@PathVariable("id") Integer id) {
		return R.ok(oilBannerService.getById(id));
	}

	@Operation(summary = "新增Banner图表", description = "新增Banner图表")
	@SysLog("新增Banner图表")
	@PostMapping
	@PreAuthorize("@pms.hasPermission('smart_oilbanner_add')")
	public R save(@RequestBody OilBanner oilBanner) {
		return R.ok(oilBannerService.save(oilBanner));
	}

	@Operation(summary = "修改Banner图表", description = "修改Banner图表")
	@SysLog("修改Banner图表")
	@PutMapping
	@PreAuthorize("@pms.hasPermission('smart_oilbanner_edit')")
	public R updateById(@RequestBody OilBanner oilBanner) {
		return R.ok(oilBannerService.updateById(oilBanner));
	}

	@Operation(summary = "通过id删除Banner图表", description = "通过id删除Banner图表")
	@SysLog("通过id删除Banner图表")
	@DeleteMapping("/{id}")
	@PreAuthorize("@pms.hasPermission('smart_oilbanner_del')")
	public R removeById(@PathVariable Integer id) {
		return R.ok(oilBannerService.removeById(id));
	}

	@Operation(summary = "启用/禁用Banner", description = "启用/禁用Banner")
	@SysLog("启用/禁用Banner")
	@PostMapping("/able/{id}")
	@PreAuthorize("@pms.hasPermission('smart_oilbanner_able')")
	public R changeLockFlagById(@PathVariable Integer id) {
		Boolean flag = oilBannerService.changeLockFlagById(id);
		if(flag){
			return R.ok("修改成功");
		}
		return R.ok("修改失败");
	}

	@Operation(summary = "批量删除轮播图", description = "批量删除轮播图")
	@SysLog("批量删除轮播图")
	@PostMapping("/batchdel")
	@PreAuthorize("@pms.hasPermission('smart_oilbanner_batch_del')")
	public R batchDelete(@RequestBody OilBannerBatchDelReq req) {
		Boolean b = oilBannerService.batchDeleteByIds(req.getIds());
		if(b){
			return R.ok("删除成功");
		}
		return R.failed("删除失败");
	}

}
