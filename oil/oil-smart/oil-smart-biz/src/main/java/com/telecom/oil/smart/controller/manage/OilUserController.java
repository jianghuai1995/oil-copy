/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.telecom.oil.smart.controller.manage;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.plugin.excel.annotation.ResponseExcel;
import com.telecom.oil.common.core.util.R;
import com.telecom.oil.common.log.annotation.SysLog;
import com.telecom.oil.smart.api.entity.OilUser;
import com.telecom.oil.smart.api.vo.OilUserExcelVo;
import com.telecom.oil.smart.api.vo.OilUserSimpleVo;
import com.telecom.oil.smart.service.OilUserService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户信息表
 *
 * @author jianghuai
 * @date 2022-10-18 14:39:08
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/oiluser")
@Tag(name = "用户信息表管理")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class OilUserController {

	private final OilUserService oilUserService;

	@Operation(summary = "分页查询", description = "分页查询")
	@GetMapping("/page")
	@PreAuthorize("@pms.hasPermission('smart_oiluser_get')")
	public R getUserInfoPage(Page page, OilUser oilUser) {
		return R.ok(oilUserService.page(page, Wrappers.query(oilUser)));
	}

	@Operation(summary = "通过id查询", description = "通过id查询")
	@GetMapping("/{id}")
	@PreAuthorize("@pms.hasPermission('smart_oiluser_get')")
	public R getById(@PathVariable("id") Long id) {
		return R.ok(oilUserService.getById(id));
	}

	@Operation(summary = "新增用户信息", description = "新增用户信息表")
	@SysLog("新增用户信息表")
	@PostMapping
	@PreAuthorize("@pms.hasPermission('smart_oiluser_add')")
	public R save(@RequestBody OilUser oilUser) {
		return R.ok(oilUserService.save(oilUser));
	}

	@Operation(summary = "修改用户信息", description = "修改用户信息表")
	@SysLog("修改用户信息表")
	@PutMapping
	@PreAuthorize("@pms.hasPermission('smart_oiluser_edit')")
	public R updateById(@RequestBody OilUser oilUser) {
		return R.ok(oilUserService.updateById(oilUser));
	}

	@Operation(summary = "通过id删除用户信息", description = "通过id删除用户信息表")
	@SysLog("通过id删除用户信息表")
	@DeleteMapping("/{id}")
	@PreAuthorize("@pms.hasPermission('smart_oiluser_del')")
	public R removeById(@PathVariable Long id) {
		return R.ok(oilUserService.removeById(id));
	}

	@ResponseExcel
	@Operation(summary = "导出excel 表格", description = "导出excel 表格")
	@GetMapping("/export")
	@PreAuthorize("@pms.hasPermission('smart_oiluser_export')")
	public List<OilUserExcelVo> export() {
		return oilUserService.listUser();
	}

	@Operation(summary = "根据电话号码查询用户简单信息", description = "根据电话号码查询用户简单信息")
	@GetMapping("/check/phone")
	public R<OilUserSimpleVo> getSimpleUserInfoByPhone(@RequestParam("phone") String phone) {
		return R.ok(oilUserService.getSimpleUserInfoByPhone(phone));
	}

}
