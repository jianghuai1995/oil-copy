/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.telecom.oil.smart.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sun.org.apache.xpath.internal.operations.Bool;
import com.telecom.oil.common.core.exception.BusinessException;
import com.telecom.oil.smart.api.entity.OilBanner;
import com.telecom.oil.smart.api.enums.LockFlagEnum;
import com.telecom.oil.smart.mapper.OilBannerMapper;
import com.telecom.oil.smart.service.OilBannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Banner图表
 *
 * @author jianghuai
 * @date 2022-11-04 14:32:47
 */
@Service
public class OilBannerServiceImpl extends ServiceImpl<OilBannerMapper, OilBanner> implements OilBannerService {

	@Autowired
	private OilBannerMapper oilBannerMapper;

	@Override
	public List<OilBanner> bannerCList() {
		return oilBannerMapper.bannerCList();
	}

	@Override
	public Boolean changeLockFlagById(Integer id) {
		OilBanner oilBanner = Optional.ofNullable(oilBannerMapper.selectOilBannerById(id)).orElseThrow(() -> new BusinessException("没查到此banner"));
		Integer lockFlag = oilBanner.getLockFlag()^1;
		return oilBannerMapper.updateLockFlagById(id, lockFlag);
	}

	@Override
	public Boolean batchDeleteByIds(List<Integer> ids) {
		if(CollectionUtil.isEmpty(ids)){
			throw new BusinessException("参数为空");
		}
		return this.removeBatchByIds(ids);
	}

}
