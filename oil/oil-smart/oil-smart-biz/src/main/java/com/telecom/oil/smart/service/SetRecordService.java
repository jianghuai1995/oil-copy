/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.telecom.oil.smart.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.telecom.oil.common.security.service.OilUser;
import com.telecom.oil.smart.api.dto.SetRecordReq;
import com.telecom.oil.smart.api.dto.SetRecordWithMarketReq;
import com.telecom.oil.smart.api.entity.SetRecord;
import com.telecom.oil.smart.api.util.PageUtils;
import com.telecom.oil.smart.api.vo.SetRecordExcelVo;
import com.telecom.oil.smart.api.vo.SetRecordMapVo;
import com.telecom.oil.smart.api.vo.SetRecordVo;

import java.util.List;

/**
 * 套餐信息调查表
 *
 * @author jianghuai
 * @date 2023-02-07 11:55:00
 */
public interface SetRecordService extends IService<SetRecord> {

	PageUtils getSetRecordPage(SetRecordReq req);

	SetRecordVo getSetRecordById(Long id);

	boolean updateSetRecordById(SetRecord setRecord);

    List<SetRecordExcelVo> listRecord();

	IPage pageQuery(Page page, QueryWrapper<SetRecord> query);

	SetRecordVo getSetRecordByCondition(Integer level2Code, Integer level3Code, Integer houseGroup, Integer houseNumber);

	PageUtils findMySetRecord(SetRecordReq req);

	List<SetRecordMapVo> listAllSetRecord();

	void uploadSetRecord(SetRecordWithMarketReq req, OilUser user);
}
