/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.telecom.oil.smart.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.telecom.oil.smart.api.dto.OilRecordReq;
import com.telecom.oil.smart.api.entity.OilRecord;
import com.telecom.oil.smart.api.entity.OilUser;
import com.telecom.oil.smart.api.util.PageUtils;
import com.telecom.oil.smart.api.vo.OilRecordExcelVo;
import com.telecom.oil.smart.api.vo.OilRecordMapVo;
import com.telecom.oil.smart.api.vo.OilRecordVo;
import com.telecom.oil.smart.mapper.OilRecordMapper;
import com.telecom.oil.smart.service.OilRecordService;
import com.telecom.oil.smart.service.OilUserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 加油记录表
 *
 * @author jianghuai
 * @date 2022-10-18 14:39:08
 */
@Slf4j
@Service
@AllArgsConstructor
public class OilRecordServiceImpl extends ServiceImpl<OilRecordMapper, OilRecord> implements OilRecordService {

	@Autowired
	private OilUserService oilUserService;
	@Autowired
	private OilRecordMapper oilRecordMapper;

	@Override
	public IPage pageQuery(Page page, QueryWrapper<OilRecord> query) {
		Page<OilRecord> pageData = this.page(page, query);
		List<OilRecordVo> vos = pageData.getRecords().stream().map(item -> {
			OilRecordVo vo = new OilRecordVo();
			BeanUtils.copyProperties(item, vo);
			//填充用户名
			OilUser oilUser = oilUserService.findById(item.getUserId());
			vo.setUserName(oilUser != null ? oilUser.getUserName() : null);
			return vo;
		}).collect(Collectors.toList());
		Page<OilRecordVo> returnPage = new Page<>();
		returnPage.setRecords(vos);
		returnPage.setTotal(pageData.getTotal());
		return returnPage;
	}

	/**
	 * 微信小程序端-分页查询加油记录
	 */
	@Override
	public PageUtils getOilRecordPage(OilRecordReq req) {
		LambdaQueryWrapper<OilRecord> lambdaQuery = Wrappers.lambdaQuery();
		lambdaQuery.like(StringUtils.isNotEmpty(req.getCarNumber()), OilRecord::getCarNumber, req.getCarNumber());
		//获取两个月以内的数据
		LocalDate lastTwoMonthDate = LocalDate.now().minus(2, ChronoUnit.MONTHS);
		lambdaQuery.gt(OilRecord::getCreateTime, lastTwoMonthDate);
		lambdaQuery.orderByDesc(OilRecord::getCreateTime);
		Page<OilRecord> page = this.page(new Page<>(req.getCurrent(), req.getSize()), lambdaQuery);
		List<OilRecordVo> voList = page.getRecords().stream().map(item -> {
			OilRecordVo vo = new OilRecordVo();
//			BeanUtils.copyProperties(item, vo);
			vo.setId(item.getId());
			vo.setSiteImage(item.getSiteImage());
			vo.setCarNumber(item.getCarNumber());
			vo.setAmount(item.getAmount());
			vo.setStationName(item.getStationName());
			vo.setCreateTime(item.getCreateTime());

//			OilUserInfo oilUserInfo = oilUserInfoService.findById(item.getUserId());
//			vo.setUserName(oilUserInfo.getUserName() != null ? oilUserInfo.getUserName() : null);
			return vo;
		}).sorted(new Comparator<OilRecordVo>() {
			@Override
			public int compare(OilRecordVo o1, OilRecordVo o2) {
				return -(o1.getCreateTime().compareTo(o2.getCreateTime()));
			}
		}).collect(Collectors.toList());
		return new PageUtils(voList, (int) page.getTotal(), (int) page.getSize(), (int) page.getCurrent());
	}

	@Override
	public OilRecordVo getOilRecordById(Long id) {
		OilRecord record = this.getById(id);
		if (record == null) {
			return null;
		}
		OilUser oilUser = oilUserService.findById(record.getUserId());
		OilRecordVo vo = new OilRecordVo();
		BeanUtils.copyProperties(record, vo);
		vo.setUserName(oilUser.getUserName());
		vo.setUserPhone(oilUser.getPhone());
		return vo;
	}

	@Override
	public OilRecordVo getOilRecordDetailCById(Long id) {
//		oilRecordMapper.selectOilRecordDetailById(id);
		return null;
	}

	@Override
	public Boolean upLoadOilRecord(OilRecord oilRecord) {
		return this.save(oilRecord);
	}

	@Override
	public PageUtils findMyOilRecord(OilRecordReq req) {
		LambdaQueryWrapper<OilRecord> lambdaQuery = Wrappers.lambdaQuery();
		lambdaQuery.like(StringUtils.isNotEmpty(req.getStation()), OilRecord::getStationName, req.getStation());
		lambdaQuery.eq(OilRecord::getUserId, req.getUserId());
		//获取一年以内的数据
		LocalDate lastYearDate = LocalDate.now().minus(1, ChronoUnit.YEARS);
		lambdaQuery.gt(OilRecord::getCreateTime, lastYearDate);
		lambdaQuery.orderByDesc(OilRecord::getCreateTime);
		Page<OilRecord> page = this.page(new Page<>(req.getCurrent(), req.getSize()), lambdaQuery);
//		OilUserInfo oilUserInfo = oilUserInfoService.getById(req.getUserId());
		List<OilRecordVo> voList = page.getRecords().stream().map(item -> {
			OilRecordVo vo = new OilRecordVo();
//			BeanUtils.copyProperties(item, vo);
			vo.setId(item.getId());
			vo.setSiteImage(item.getSiteImage());
			vo.setCarNumber(item.getCarNumber());
			vo.setAmount(item.getAmount());
			vo.setStationName(item.getStationName());
			vo.setCreateTime(item.getCreateTime());

//			vo.setUserName(oilUserInfo.getUserName() != null ? oilUserInfo.getUserName() : null);
			return vo;
		}).sorted(new Comparator<OilRecordVo>() {
			@Override
			public int compare(OilRecordVo o1, OilRecordVo o2) {
				return -(o1.getCreateTime().compareTo(o2.getCreateTime()));
			}
		}).collect(Collectors.toList());
		return new PageUtils(voList, (int) page.getTotal(), (int) page.getSize(), (int) page.getCurrent());
	}

	@Override
	public List<OilRecordExcelVo> listRecord() {
		List<OilRecord> recordList = this.list(Wrappers.emptyWrapper());
		// 转换成execl 对象输出
		return recordList.stream().map(record -> {
			OilRecordExcelVo oilRecordExcelVO = new OilRecordExcelVo();
			BeanUtil.copyProperties(record, oilRecordExcelVO);
			OilUser oilUser = oilUserService.getById(record.getUserId());
			if (oilUser != null) {
				oilRecordExcelVO.setUserName(oilUser.getUserName());
			}
			return oilRecordExcelVO;
		}).collect(Collectors.toList());
	}

	/**
	 * 地图应用展示加油记录
	 * @return
	 */
	@Override
	public List<OilRecordMapVo> listOilRecordMap() {
		return oilRecordMapper.listOilRecordMap().stream().map(item -> {
			return OilRecordMapVo.builder()
					.id(item.getId())
					.carNumber(item.getCarNumber())
					.longitude(item.getLongitude())
					.latitude(item.getLatitude()).build();
		}).collect(Collectors.toList());
	}

}
