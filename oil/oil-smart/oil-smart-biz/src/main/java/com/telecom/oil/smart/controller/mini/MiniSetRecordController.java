package com.telecom.oil.smart.controller.mini;

import com.telecom.oil.common.core.util.R;
import com.telecom.oil.common.security.annotation.Inner;
import com.telecom.oil.smart.api.dto.SetRecordReq;
import com.telecom.oil.smart.api.dto.SetRecordWithMarketReq;
import com.telecom.oil.smart.api.entity.OilUser;
import com.telecom.oil.smart.api.entity.SetRecord;
import com.telecom.oil.smart.api.enums.RecognizeStatusEnum;
import com.telecom.oil.smart.api.vo.SetRecordVo;
import com.telecom.oil.smart.service.OilUserService;
import com.telecom.oil.smart.service.SetRecordService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/mini/set/record")
@Tag(name = "微信小程序端-套餐模块")
@Inner(value = false)
@RequiredArgsConstructor
public class MiniSetRecordController extends MiniBaseController{

	private final SetRecordService setRecordService;
	private final OilUserService oilUserService;

	@Operation(summary = "分页查询套餐", description = "分页查询套餐")
	@GetMapping("/page")
	public R getSetRecordPage(HttpServletRequest request, SetRecordReq req) {
		checkUserWithFullRight(request);
		return R.ok(setRecordService.getSetRecordPage(req));
	}

	@Operation(summary = "通过id查询套餐详情", description = "通过id查询套餐详情")
	@GetMapping("/{id}")
	public R<SetRecordVo> getSetRecordById(HttpServletRequest request, @PathVariable("id") Long id) {
		checkUserWithFullRight(request);
		return R.ok(setRecordService.getSetRecordById(id));
	}

	@Operation(summary = "上报套餐", description = "上报套餐")
	@PostMapping("/add")
	public R save(HttpServletRequest request, @RequestBody SetRecordWithMarketReq req) {
		com.telecom.oil.common.security.service.OilUser user = checkUserWithFullRight(request);
		setRecordService.uploadSetRecord(req, user);
		return R.ok("上报成功");
	}

	@Operation(summary = "根据乡镇/街道、村/社区、组别、门牌号查询最近一条记录", description = "根据乡镇/街道、村/社区、组别、门牌号查询最近一条记录")
	@GetMapping("/recent")
	public R<SetRecordVo> getSetRecordByCondition(HttpServletRequest request,
										   @RequestParam("level2Code") int level2Code,
										   @RequestParam("level3Code") int level3Code,
										   @RequestParam("houseGroup") int houseGroup,
										   @RequestParam("houseNumber") int houseNumber) {
		checkUserWithFullRight(request);
		return R.ok(setRecordService.getSetRecordByCondition(level2Code,level3Code,houseGroup,houseNumber));
	}

	@Operation(summary = "我的套餐记录", description = "我的套餐记录")
	@GetMapping("/mysetrecord")
	public R findMySetRecord(HttpServletRequest request, SetRecordReq req) {
		com.telecom.oil.common.security.service.OilUser user = checkUserWithFullRight(request);
		// 实时判定用户认证状态
		OilUser userInfo = oilUserService.getById(user.getId());
		if (!userInfo.getRecognizeStatus().equals(RecognizeStatusEnum.RECOGNIZED.getCode())) {
			return R.failed("未通过认证");
		}
		req.setUserId(user.getId());
		return R.ok(setRecordService.findMySetRecord(req));
	}

	@Operation(summary = "更新套餐", description = "更新套餐")
	@PostMapping("/update")
	public R updateById(HttpServletRequest request, @RequestBody SetRecord setRecord) {
		checkUserWithFullRight(request);
		return R.ok(setRecordService.updateSetRecordById(setRecord));
	}

	@Operation(summary = "地图展示套餐记录", description = "地图展示套餐记录")
	@GetMapping("/map/list")
	public R listAllSetRecord(HttpServletRequest request) {
		checkUserWithFullRight(request);
		return R.ok(setRecordService.listAllSetRecord());
	}

}
