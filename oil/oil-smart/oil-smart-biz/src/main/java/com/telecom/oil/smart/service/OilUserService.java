/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.telecom.oil.smart.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.telecom.oil.smart.api.dto.RecognizeReq;
import com.telecom.oil.smart.api.entity.OilUser;
import com.telecom.oil.smart.api.vo.OilUserExcelVo;
import com.telecom.oil.smart.api.vo.OilUserSimpleVo;
import com.telecom.oil.smart.api.vo.OilUserVo;

import java.util.List;

/**
 * 用户信息表
 *
 * @author jianghuai
 * @date 2022-10-18 14:39:08
 */
public interface OilUserService extends IService<OilUser> {

	String recognize(RecognizeReq req);

	OilUser weixinLogin(String code);

	void identifyApply(Long id);

	List<OilUserExcelVo> listUser();

	OilUserVo getUserInfo(Long id);

	OilUser findById(Long userId);

	OilUserSimpleVo getSimpleUserInfoByPhone(String phoneNumber);

}
