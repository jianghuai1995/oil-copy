/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.telecom.oil.smart.controller.manage;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.plugin.excel.annotation.ResponseExcel;
import com.telecom.oil.common.core.util.R;
import com.telecom.oil.common.log.annotation.SysLog;
import com.telecom.oil.smart.api.entity.OilRecord;
import com.telecom.oil.smart.api.vo.OilRecordExcelVo;
import com.telecom.oil.smart.service.OilRecordService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 加油记录表
 *
 * @author jianghuai
 * @date 2022-10-18 14:39:08
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/oilrecord")
@Tag(name = "加油记录表管理")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class OilRecordController {

	private final OilRecordService oilRecordService;

	@Operation(summary = "分页查询", description = "分页查询")
	@GetMapping("/page")
	@PreAuthorize("@pms.hasPermission('smart_oilrecord_get')")
	public R getOilRecordPage(Page page, OilRecord oilRecord) {
		return R.ok(oilRecordService.pageQuery(page, Wrappers.query(oilRecord)));
	}

	@Operation(summary = "通过id查询", description = "通过id查询")
	@GetMapping("/{id}")
	@PreAuthorize("@pms.hasPermission('smart_oilrecord_get')")
	public R getOilRecordById(@PathVariable("id") Long id) {
		return R.ok(oilRecordService.getOilRecordById(id));
	}

	@Operation(summary = "新增加油记录", description = "新增加油记录")
	@SysLog("新增加油记录表")
	@PostMapping
	@PreAuthorize("@pms.hasPermission('smart_oilrecord_add')")
	public R save(@RequestBody OilRecord oilRecord) {
		return R.ok(oilRecordService.save(oilRecord));
	}

	@Operation(summary = "修改加油记录", description = "修改加油记录")
	@SysLog("修改加油记录表")
	@PutMapping
	@PreAuthorize("@pms.hasPermission('smart_oilrecord_edit')")
	public R updateById(@RequestBody OilRecord oilRecord) {
		return R.ok(oilRecordService.updateById(oilRecord));
	}

	@Operation(summary = "通过id删除加油记录", description = "通过id删除加油记录")
	@SysLog("通过id删除加油记录表")
	@DeleteMapping("/{id}")
	@PreAuthorize("@pms.hasPermission('smart_oilrecord_del')")
	public R removeById(@PathVariable Long id) {
		return R.ok(oilRecordService.removeById(id));
	}

	@ResponseExcel
	@Operation(summary = "导出excel 表格", description = "导出excel 表格")
	@GetMapping("/export")
	@PreAuthorize("@pms.hasPermission('smart_oilrecord_export')")
	public List<OilRecordExcelVo> export() {
		return oilRecordService.listRecord();
	}

}
