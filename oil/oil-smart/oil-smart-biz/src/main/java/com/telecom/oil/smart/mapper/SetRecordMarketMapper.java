/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the oil4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.telecom.oil.smart.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.telecom.oil.smart.api.entity.SetRecordMarket;
import org.apache.ibatis.annotations.Mapper;

/**
 * 营销套餐表
 *
 * @author jianghuai
 * @date 2023-05-01 21:13:52
 */
@Mapper
public interface SetRecordMarketMapper extends BaseMapper<SetRecordMarket> {

}
