package com.telecom.oil.smart.controller.mini;

import com.telecom.oil.common.core.util.R;
import com.telecom.oil.common.security.annotation.Inner;
import com.telecom.oil.smart.api.dto.OcrRecognizeReq;
import com.telecom.oil.smart.service.AliyunOcrRecognizeBasicService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/mini/ocr")
@Tag(name = "微信小程序端-OCR识别模块")
@Inner(value = false)
@RequiredArgsConstructor
public class MiniOCRController extends MiniBaseController{

	private final AliyunOcrRecognizeBasicService ocrRecognizeBasicService;


	@Operation(summary = "OCR通用文字识别")
	@PostMapping(value = "/recognize")
	public R OCRBasicRecognize(HttpServletRequest request,
							   @RequestBody OcrRecognizeReq req) {
		checkUserWithFullRight(request);
		return R.ok(ocrRecognizeBasicService.recognizeBasic(req.getUrl()));
	}

}
