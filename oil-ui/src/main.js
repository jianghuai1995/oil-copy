import { createApp } from 'vue'
import 'element-plus/dist/index.css'
import '@smallwei/avue/lib/index.css';
import animate from 'animate.css'
import 'styles/common.scss';
import website from '@/config/website.js'
import axios from '@/router/axios.js';
import router from '@/router/index.js';
import store from '@/store/index.js';
import i18n from '@/lang/index.js';
import { getScreen, downBlobFile } from '@/util/index.js'
import { baseUrl } from "@/config/env.js";
import dict from '@/util/dict.js'
import '@/permission.js';
import error from '@/error.js';
import ElementPlus from 'element-plus'
import Avue from '@smallwei/avue';
import AvueFormDesign from 'avue-form-design';
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import basicBlock from 'components/BasicBlock/main.vue'
import basicContainer from 'components/BasicContainer/main.vue'
import DictTag from '@/components/DictTag/index.vue'
import App from './App.vue'
import Viewer from 'v-viewer'
import 'viewerjs/dist/viewer.css'
//引入高德地图
// import VueAMap from 'vue-amap'

window.axios = axios;
const app = createApp(App)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}
//注册全局容器
app.component('basicContainer', basicContainer)
app.component('basicBlock', basicBlock)
app.component('DictTag', DictTag)

app.config.globalProperties.website = website
app.config.globalProperties.baseUrl = baseUrl
app.config.globalProperties.getScreen = getScreen
app.config.globalProperties.downBlobFile = downBlobFile
dict(app);
app.use(error);
app.use(i18n)
app.use(animate)
app.use(store)
app.use(router)
app.use(ElementPlus)
app.use(Avue, {
  axios,
  tableSize: 'small'
})
app.use(AvueFormDesign)

app.use(Viewer)
// app.use(VueAMap)
// VueAMap.initAMapApiLoader({
//   key: '0c6342bee19c65a7d6446dfeeb08167d',
//   plugin: ['AMap.Autocomplete',
//       'AMap.PlaceSearch',
//       'AMap.Scale',
//       'AMap.OverView',
//       'AMap.ToolBar',
//       'AMap.MapType',
//       'AMap.PolyEditor',
//       'AMap.CircleEditor',
//       'AMap.Geocoder'
//   ],
//   // 默认高德 sdk 版本为 1.4.4
//   v: '1.4.4'
// })
// window._AMapSecurityConfig = {
//   securityJsCode: 'd84a7d1d0021a8f3ced3c855a678bcbc'
// }

app.mount('#app')
