import App from './App'
import Vue from 'vue'
import uView from '@/uni_modules/uview-ui'

Vue.config.productionTip = false
App.mpType = 'app'

Vue.use(uView)

// 全局存储 vuex 的封装
import store from '@/store';
import share from '@/common/share.js'

// 引入 uView 提供的对 vuex 的简写法文件
let vuexStore = require('@/store/$u.mixin.js');
Vue.mixin(vuexStore);
Vue.mixin(share)
	
const app = new Vue({
	store,
	share,
    ...App
}) 

// 引入请求封装，将app参数传递到配置中, 及开启请求拦截器
require('@/common/request.js')(app)

// api集中管理挂在到vue上
import httpApi from '@/common/api.js'
Vue.use(httpApi, app)

app.$mount()
