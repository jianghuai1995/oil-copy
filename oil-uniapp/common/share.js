export default {
    data() {
       return {
            share:{
               title: '一站式记账服务小程序',
               imageUrl:'https://www.xxxx.top/oil-smart/20221218/share.jpg',
            }
       }
    },
    onShareAppMessage(res) { //发送给朋友
        return {
            title: this.share.title,
            imageUrl: this.share.imageUrl,
        }
    },
    onShareTimeline(res) {//分享到朋友圈
        return {
            title: this.share.title,
            imageUrl: this.share.imageUrl,
        }
    },
 
 
}