//挂载api到vue中
const http = uni.$u.http;
// 此处第二个参数vm，就是我们在页面使用的this，你可以通过vm获取vuex等操作，更多内容详见uView对拦截器的介绍部分：
//注意这里的install不能修改
const install = (Vue, vm) => {
	// 将各个定义的接口名称，统一放进对象挂载到vm.$u.api(因为vm就是this，也即this.$u.api)下
	vm.$u.api = {
		
		//banner相关接口
		banner:{
			bannerList: (params = {}) => http.get('/smart/mini/banner/list', params), //banner图列表
		},
		
		//通知相关接口
		notice:{
			noticeList: (params = {}) => http.get('/smart/mini/notice/page', params), //通知列表
			noticeDetail: (params = {}) => http.get('/smart/mini/notice/' + params), //通知详情
			noticeLike: (params = {}) => http.get('/smart/mini/notice/like/'+ params), //通知详情点赞
		},
		
		//OCR识别
		ocr:{
			recognize: (params = {}) => http.post('/smart/mini/ocr/recognize', params), //图片OCR识别
		},
		
		//车牌相关接口
		car:{
			carNumberList: (params = {}) => http.get('/smart/mini/oilcar/list', params), //车牌号列表
		},
		
		//加油记录相关接口
		record:{
			recordList: (params = {}) => http.get('/smart/mini/oilrecord/page', params), //加油列表
			recordDetail: (params = {}) => http.get('/smart/mini/oilrecord/' + params), //加油详情
			recordLike: (params = {}) => http.get('/smart/mini/oilrecord/like/'+ params), //加油详情点赞
			recordAdd: (params = {}) => http.post('/smart/mini/oilrecord/add', params), //上传加油
			recordDelete: (params = {}) => http.get('/smart/mini/oilrecord/delete/' + params), //加油记录撤销
			myRecordList: (params = {}) => http.get('/smart/mini/oilrecord/myoilrecord', params), //我的加油列表
			mapRecordList: (params = {}) => http.get('/smart/mini/oilrecord/map/list', params), //所油地图列表
			carNumberList: (params = {}) => http.get('/smart/mini/oilcar/list', params), //车牌号列表
		},
		
		//数字乡村套餐相关接口
		set:{
			regionList: (params = {}) => http.get('/smart/mini/set/region/list', params), //行政区列表
			setRecordList: (params = {}) => http.get('/smart/mini/set/record/page', params), //套餐列表
			setRecordDetail: (params = {}) => http.get('/smart/mini/set/record/' + params), //套餐详情
			setRecordAdd: (params = {}) => http.post('/smart/mini/set/record/add', params), //上传套餐
			mySetRecordList: (params = {}) => http.get('/smart/mini/set/record/mysetrecord', params), //我的套餐列表
			queryRecent: (params = {}) => http.get('/smart/mini/set/record/recent', params), //查询最近一条记录
			mapSetRecordList: (params = {}) => http.get('/smart/mini/set/record/map/list', params), //套餐地图列表
		},
		
		//我的相关接口
		mine:{
			weixinLogin: (params = {}, header = {}) => http.post('/auth/oauth2/token', params, header), //微信一键登录
			logout: (params = {}) => http.delete('/auth/token/logout', params), //退出登录
			identifyApply: (params = {}) => http.get('/smart/mini/userinfo/identify', params), //会员认证
			uploadFile: (params = {}) => http.uploadFile('/smart/mini/file/upload', params), //上传文件
			userinfo: (params = {}) => http.get('/smart/mini/userinfo/info'), //获取用户信息
			updateUserInfo: (params = {}, header = {}) => http.post('/smart/mini/userinfo/update', params, header) //更新用户信息
		}
		
	};
	
}

export default {
	install
}
