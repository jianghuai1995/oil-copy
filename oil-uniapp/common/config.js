const config = {

	// 产品名称
	productName: 'Oil Applet',

	// 公司名称
	companyName: 'Talent INC',

	// 产品版本号
	productVersion: 'V1.1',

	// 版本检查标识
	appCode: 'android',

	// 内部版本号码
	appVersion: 1,

}
 
config.baseUrl = 'https://www.xxxx.top'; // 设置后台接口服务的基础地址
// config.baseUrl = 'http://127.0.0.1:9999'; // 设置后台接口服务的基础地址

//地图默认展示经纬度 - 梁平
config.baseLongitude = Number(107.76229858398438) // 经度
config.baseLatitude = Number(30.638954162597656) // 纬度

// 建议：打开下面注释，方便根据环境，自动设定服务地址
if (process.env.NODE_ENV === 'development') {
		
}

export default config;
