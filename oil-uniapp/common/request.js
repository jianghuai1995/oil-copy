import commonConfig from "../common/config.js";

// 此vm参数为页面的实例，可以通过它引用vuex中的变量
module.exports = (vm) => {

	// 初始化请求配置
	uni.$u.http.setConfig((config) => {
		/* config 为默认全局配置*/
		config.baseURL = commonConfig.baseUrl; /* 根域名 */
		config.dataType = 'json'; //设置为json，返回后会对数据进行一次JSON.parse()
		config.showLoading = true; //是否显示请求中的loading
		config.loadingText = '请求中...'; //请求loading中的文字提示
		config.loadingTime = 800; // 在此时间内，请求还没回来的话，就显示加载中动画，单位ms
		config.originalData = true; // 是否在拦截器中返回服务端的原始数据
		config.loadingMask = true; // 展示loading的时候，是否给一个透明的蒙层，防止触摸穿透
		// 配置请求头信息
		config.header = {
			'x-requested-with': 'XMLHttpRequest'
		};
		//请求超时时间
		config.timeout = 15000;
		// 全局自定义验证器。参数为statusCode 且必存在，不用判断空情况。
		config.validateStatus = (statusCode) => { // statusCode 必存在。此处示例为全局默认配置
			return statusCode >= 200 && statusCode < 300
		}
		return config
	})

	// 请求拦截
	uni.$u.http.interceptors.request.use((config) => { // 可使用async await 做异步操作
		// if(vm.vuex_token && vm.vuex_token !== 'YXBwbGV0OmFwcGxldA=='){
		// 	config.header.Authorization = 'Bearer ' + vm.vuex_token
		// }
		const token = uni.getStorageSync('token');
		// 配置token字段
		if(token){
			config.header.Authorization = 'Bearer ' + token
		}

		// 初始化请求拦截器时，会执行此方法，此时data为undefined，赋予默认{}
		config.data = config.data || {}
		// 可以对某个url进行特别处理，此url参数为this.$u.get(url)中的url值
		// if (config.url == '/auth/oauth2/token') config.header.noToken = true;
		// 最后需要将config进行return
		return config;
		// 如果return一个false值，则会取消本次请求
		// if(config.url == '/user/rest') return false; // 取消某次请求
	}, config => { // 可使用async await 做异步操作
		return Promise.reject(config)
	})

	// 响应拦截
	uni.$u.http.interceptors.response.use((response) => {
		/* 对响应成功做点什么 可使用async await 做异步操作*/
		const {
			data,
			statusCode
		} = response;
		// const data = response.data;

		// 自定义参数
		const custom = response.config?.custom
		if (statusCode !== 200) {
			// 如果没有显式定义custom的toast参数为false的话，默认对报错进行toast弹出提示
			//用于全局报错提示
			// if (custom.toast !== false) {
			// 	uni.$u.toast(data)
			// }

			// 如果需要catch返回，则进行reject
			if (custom?.catch) {
				return Promise.reject(data)
			} else {
				// 否则返回一个pending中的promise，请求不会进入catch中
				return new Promise(() => {})
			}
		}
		// return data.data === undefined ? {} : data.data
		return data
	}, (response) => {
		//发生网络错误时会走到这，可以在这统一设置catch操作，页面中就不用再进行catch
		// 对响应错误做点什么 （statusCode !== 200）
		//reject方法是promise操作异常，执行catch
		return Promise.reject(response)
	})
}
